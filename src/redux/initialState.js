export const INITIAL_STATE_AUTH = {
    email: '',
    password: '',
    user: null,
    error: '',
    loading: false
};

export const INITIAL_STATE_POSTS = {
    posted: {},
    likedList: {},
    loading: false,
    uploading: false,
    singlePost: {},
    allKeys: []
};

export const INITIAL_STATE_COMMENTS = {
    comments: {},
    loading: false,
    postId: null,
    userId: null,
    currentComment: ''
};

export const INITIAL_STATE_CATEGORIES = {
    all: {},
    selected: '',
    postsByCategory: {},
    loading: false
};

export const INITIAL_STATE_REGISTRATION = {
    imageUri: '',
    email: '',
    password: '',
    username: '',
    user: null,
    loading: false,
    error: ''
};

export const INITIAL_STATE_CHAT = {
    contacts: [],
    selected: '',
    messages: [],
    loading: false,
    imageUploading: false,
    errorText: ''
};

export const INITIAL_STATE_USER_INFO = {
    userId: '',
    displayName: '',
    photoURL: '',
    backgroundPhotoURL: '',
    moderationPostsCount: 0,
    likes: {},
    about: '',
    loading: false,
    notifications: {}
};
