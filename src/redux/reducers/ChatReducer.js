import C from "../actions/types";
import {INITIAL_STATE_CHAT} from "../initialState";


export default (state = INITIAL_STATE_CHAT, action) => {
    switch (action.type) {
        case C.LOAD_CONTACTS:
            return {
                ...state,
                loading: true
            };
        case C.CONTACTS_LOADED:
            return {
                ...state,
                contacts: action.payload,
                loading: false
            };
        case C.LOAD_MESSAGES:
            return {
                ...state,
                loading: true
            };
        case C.MESSAGES_LOADED:
            return {
                ...state,
                messages: action.payload,
                loading: false
            };
        case C.CONTACT_SELECTED:
            return {
                ...state,
                selected: action.payload
            };
        case C.A_POSTS_BY_CAT_LOADED:
            return {
                ...state,
                postsByCategory: action.payload,
                loading: false
            };
        case C.CHAT_IMG_UPLOADING:
            return {
                ...state,
                imageUploading: true
            };
        case C.CHAT_IMG_SUCCESS:
            return {
                ...state,
                imageUploading: false
            };
        case C.CHAT_IMG_ERROR:
            return {
                ...state,
                imageUploading: false,
                errorText: action.payload
            };
        default:
            return state;
    }
};
