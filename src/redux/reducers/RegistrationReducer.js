import C from "../actions/types";
import { INITIAL_STATE_REGISTRATION } from "../initialState";


export default (state = INITIAL_STATE_REGISTRATION, action) => {
    switch (action.type) {
        case C.REG_EMAIL_CHANGED:
            return {...state, email: action.payload};
        case C.REG_IMAGEURI_CHANGED:
            return {...state, imageUri: action.payload};
        case C.REG_USERNAME_CHANGED:
            return {...state, username: action.payload};
        case C.REG_PASSWORD_CHANGED:
            return {...state, password: action.payload};
        case C.REG_SUCCESS:
            return {...state, ...INITIAL_STATE_REGISTRATION, user: action.payload};
        case C.REG_TRY:
            return {...state, loading: true, error: ''};
        case C.REG_ERROR:
            return {...state, ...INITIAL_STATE_REGISTRATION, error: action.payload};
        default:
            return state;
    }
};
