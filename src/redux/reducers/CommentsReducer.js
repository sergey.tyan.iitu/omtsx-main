import C from "../actions/types";
import {INITIAL_STATE_COMMENTS} from "../initialState";


export default (state = INITIAL_STATE_COMMENTS, action) => {
    switch (action.type) {
        case C.NEW_LOADED_COMMENT: {
            return {
                ...state,
                comments: action.payload,
                loading: false
            };
        }
        case C.LOADING_COMMENTS_BY_POST:{
            return{
                ...state,
                loading: true,
                postId: action.payload
            }
        }
        case C.LOADING_COMMENTS_BY_USER:{
            return{
                ...state,
                loading: true,
                userId: action.payload
            }
        }
        case C.COMMENT_TYPED:{
            return{
                ...state,
                currentComment: action.payload
            }
        }
        case C.COMMENT_SENT:{
            return{
                ...state,
                currentComment:''
            }
        }
        default:{
            return state;
        }
    }
};



