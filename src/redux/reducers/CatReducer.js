import C from "../actions/types";
import {INITIAL_STATE_CATEGORIES} from "../initialState";


export default (state = INITIAL_STATE_CATEGORIES, action) => {
    switch (action.type) {
        case C.A_LOAD_CATS:
            return {
                ...state,
                loading: true
            };
        case C.A_CATS_LOADED:
            return {
                ...state,
                all: action.payload,
                loading: false
            };
        case C.A_LOAD_POSTS_BY_CAT:
            return {
                ...state,
                selected: action.payload,
                loading: true
            };
        case C.A_POSTS_BY_CAT_LOADED:
            return {
                ...state,
                postsByCategory: action.payload,
                loading: false
            };

        default:
            return state;
    }
};
