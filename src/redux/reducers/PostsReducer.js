import C from '../actions/types';
import { INITIAL_STATE_POSTS } from '../initialState';

export default (state = INITIAL_STATE_POSTS, action) => {
    switch (action.type) {
        case C.NEW_POSTED_POST: {
            return {
                ...state,
                posted: action.payload,
                loading: false
            };
        }
        case C.LOADING_POSTS: {
            return {
                ...state,
                loading: true
            }
        }
        case C.UPLOADING_POST: {
            return {
                ...state,
                uploading: true
            }
        }
        case C.UPLOADED_POST: {
            return {
                ...state,
                uploading: false
            }
        }
        case C.NEW_LIKED_PERSON: {
            return {
                ...state,
                likedList: action.payload
            }
        }
        case C.SINGLE_POST_LOADED: {
            return {
                ...state,
                singlePost: action.payload
            }
        }
        default: {
            return state;
        }
    }
};



