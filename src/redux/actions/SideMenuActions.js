import {
    OPEN_BEST,
    OPEN_CATEGORIES,
    OPEN_MAIN,
    OPEN_NOTIFICATIONS,
    OPEN_PROFILE,
    OPEN_RANDOM,
    OPEN_SETTINGS,
    OPEN_MESSAGES
} from './types';

import {Actions} from "react-native-router-flux";

export const openMain = (prev) =>{
    return (dispatch) => {
        dispatch({
            type: OPEN_MAIN
        });
        Actions.news();
    }
};

export const openBest = (prev) =>{
    return (dispatch) => {
        dispatch({
            type: OPEN_BEST
        });
        Actions.best();
    }
};


export const openRandom = () =>{
    return (dispatch) => {
        dispatch({
            type: OPEN_RANDOM
        });
        Actions.random();
    }
};

export const openNotifications = () =>{
    return (dispatch) => {
        dispatch({
            type: OPEN_NOTIFICATIONS
        });
        Actions.notifications();
    }
};

export const openCategories = () =>{
    return (dispatch) => {
        dispatch({
            type: OPEN_CATEGORIES
        });
        Actions.categories();
    }
};

export const openSettings = () =>{
    return (dispatch) => {
        dispatch({
            type: OPEN_SETTINGS
        });
        Actions.settings();
    }
};

export const openMessages = () =>{
    return (dispatch) => {
        dispatch({
            type: OPEN_MESSAGES
        });
        // Actions.messages();
        Actions.contacts();
    }
};

export const openProfile = () =>{
    return (dispatch) => {
        dispatch({
            type: OPEN_PROFILE
        });
        // Actions.messages();
        Actions.account();
    }
};


