import C from './types';
import * as firebase from "firebase";

export const loadCategories = () => {
    const catsRef = firebase.database().ref(C.F_CATEGORIES);
    return dispatch => {
        dispatch({ type: C.A_LOAD_CATS});
        catsRef.once('value', snapshot => {
            dispatch({
                type: C.A_CATS_LOADED,
                payload: snapshot.val()
            })
        });
    }
};

