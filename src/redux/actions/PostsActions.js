import C from './types';
import * as firebase from 'firebase';
import moment from 'moment';
import RNFetchBlob from 'react-native-fetch-blob';
import { Notification } from '../../models/Notification';
import { NOTIFICATION_TYPE, TIME_FORMAT } from '../../constants';
import { pushNotification } from './UserInfoActions';

export const fetchPublishedPosts = (postAmount) => {
    const approvedPostsRef = firebase.database().ref(C.F_POSTED_POSTS).limitToLast(postAmount);
    return dispatch => {
        dispatch({ type: C.LOADING_POSTS });
        approvedPostsRef.on('value', snapshot => {
            dispatch({
                type: C.NEW_POSTED_POST,
                payload: snapshot.val()
            })
        });
    }
};

export const fetchPublishedPostsByCat = (postAmount, catId) => {
    const approvedPostsRef = firebase.database().ref(C.F_POSTED_BY_CAT_POSTS + catId).limitToLast(postAmount);
    return dispatch => {
        dispatch({ type: C.LOADING_POSTS });
        approvedPostsRef.on('value', snapshot => {
            dispatch({
                type: C.NEW_POSTED_POST,
                payload: snapshot.val()
            })
        });
    }
};

export const stopWatchingPublishedPostsByCat = (catId) => {
    return () => {
        firebase.database().ref(C.F_POSTED_BY_CAT_POSTS + catId).off();
    }
};

export const stopWatchingPublishedPosts = () => {
    return () => {
        firebase.database().ref(C.F_POSTED_POSTS).off();
    }
};

const Blob = RNFetchBlob.polyfill.Blob;
const fs = RNFetchBlob.fs;
window.XMLHttpRequest = RNFetchBlob.polyfill.XMLHttpRequest;
window.Blob = Blob;

export const writeNewPost = (uid, userPic, userName, title, body, category, imageUri, premium) => {
    return dispatch => {

        dispatch({
            type: C.UPLOADING_POST
        });

        let postData = {
            uid, userPic, userName, title, body, category,
            image: null
        };
        let POSTS_REF;
        if (premium) {
            POSTS_REF = C.F_POSTS_FOR_MODERATION_PREMIUM;
        } else {
            POSTS_REF = C.F_POSTS_FOR_MODERATION;
        }
        let newPostKey = firebase.database().ref().child('posts').push().key;
        let updates = {};
        if (imageUri) {
            let uploadPromise = new Promise((resolve, reject) => {
                let uploadBlob = null;
                const imageRef = firebase.storage().ref().child('images/' + newPostKey);
                const mime = 'application/octet-stream';
                fs.readFile(imageUri, 'base64')
                    .then((data) => {
                        return Blob.build(data, { type: `${mime};BASE64` })
                    })
                    .then((blob) => {
                        uploadBlob = blob
                        return imageRef.put(blob, { contentType: mime })
                    })
                    .then(() => {
                        uploadBlob.close()
                        return imageRef.getDownloadURL()
                    })
                    .then((url) => {
                        resolve(url)
                    })
                    .catch((error) => {
                        reject(error)
                    })
            });


            uploadPromise.then((url) => {
                postData.image = url;
                const imageData = {
                    src: url,
                    auhtor: uid
                };
                updates[POSTS_REF + newPostKey] = postData;
                updates[C.F_USER_POSTS + uid + '/' + newPostKey] = postData;
                updates[C.F_USER_IMAGES + uid + '/' + newPostKey] = imageData;
                updates[C.F_IMAGES + newPostKey] = imageData;
                firebase.database().ref().update(updates);
                alert('Ваша публикация будет на модерации от 1-3 дней');
                dispatch({type:C.UPLOADED_POST});
            }).catch((err) => {
                alert('error uploading image', err);
            })

        } else {
            updates[POSTS_REF + newPostKey] = postData;
            updates[C.F_USER_POSTS + uid + '/' + newPostKey] = postData;
            firebase.database().ref().update(updates);
            alert('Ваша публикация будет на модерации от 1-3 дней');
            dispatch({type:C.UPLOADED_POST});
        }

    }
};

export const toggleLike = (
    postId,
    postShort,
    postCatId,
    postAuthorId,
    likeAuthorUid,
    likeAuthorName,
    likeAuthorPhotoURL
) => {
    const postRef = firebase.database().ref(`${C.F_POSTED_POSTS}${postId}`);
    togglePostRefLike(postRef, likeAuthorUid, likeAuthorName);

    const notification = createPostLikeNotification(
        postId,
        postShort,
        likeAuthorUid,
        likeAuthorName,
        likeAuthorPhotoURL
    );
    const userRef = firebase.database().ref(`${C.F_USER_INFO}${likeAuthorUid}`);
    toggleUserRefLike(userRef, postId, postShort, notification, postAuthorId);

    if(postCatId) {
        const catPostRef = firebase.database().ref(`${C.F_POSTED_BY_CAT_POSTS}${postCatId}/${postId}`);
        togglePostRefLike(catPostRef, likeAuthorUid, likeAuthorName);
    }


};

const createPostLikeNotification = (postId, postShort, likeAuthorUid, likeAuthorName, likeAuthorPhotoURL) => {
    let notification = new Notification();
    notification.postId = postId;
    notification.postShort = postShort;
    notification.type = NOTIFICATION_TYPE.post_like;
    notification.likeAuthorName = likeAuthorName;
    notification.likeAuthorPhotoURL = likeAuthorPhotoURL;
    notification.likeAuthorUid = likeAuthorUid;
    notification.time = moment().format(TIME_FORMAT);
    return notification;
};

const toggleUserRefLike = (userRef, postId, postShort, notification, postAuthorId) => {
    userRef.transaction(user => {
        if (user) {
            if (user.likes && user.likes[postId]) {
                user.likes[postId] = null;
            } else {
                pushNotification(postAuthorId, notification);
                if (!user.likes) {
                    user.likes = {};
                }
                user.likes[postId] = postShort;
            }
        }
        return user;
    });
};

const togglePostRefLike = (postRef, uid, name) => {

    postRef.transaction(post => {
        if (post) {
            if (post.likes && post.likes[uid]) {
                post.likeCount--;
                post.likes[uid] = null;
            } else {
                if(post.likeCount === undefined){
                    post.likeCount = 1;
                }else{
                    post.likeCount++;
                }

                if (!post.likes) {
                    post.likes = {};
                }
                post.likes[uid] = name;
            }
        }
        return post;
    });
};

export const fetchLikedList = (postId, count) => {
    console.log('fetchLikedList');
    const likedListRef = firebase.database().ref(C.F_POSTED_POSTS + postId + '/likes').limitToLast(count);
    return dispatch => {
        likedListRef.once('value', snapshot => {
            dispatch({
                type: C.NEW_LIKED_PERSON,
                payload: snapshot.val()
            })
        });
    }
};

export const fetchSinglePublishedPost = (postId) => {
    const postRef = firebase.database().ref(C.F_POSTED_POSTS + postId);
    return dispatch => {
        postRef.once('value', snapshot => {
            let res = {
                ...snapshot.val(),
                postId
            };

            dispatch({
                type: C.SINGLE_POST_LOADED,
                payload: res
            })
        });
    }
};
