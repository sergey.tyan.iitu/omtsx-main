import { Actions } from 'react-native-router-flux';
import C from './types';
import * as firebase from 'firebase';
import { uploadAvatarPromise } from './photoUploadService';

export const emailChanged = (text) => {
    return {
        type: C.REG_EMAIL_CHANGED,
        payload: text
    };
};

export const imageUriChanged = (uri) => {
    return {
        type: C.REG_IMAGEURI_CHANGED,
        payload: uri
    };
};

export const usernameChanged = (username) => {
    return {
        type: C.REG_USERNAME_CHANGED,
        payload: username
    };
};

export const passwordChanged = (text) => {
    return {
        type: C.REG_PASSWORD_CHANGED,
        payload: text
    };
};

export const registerUser = (username, email, password, imageUri, gender) => {
    console.log('trying to register');
    console.log(username, email, password, imageUri, gender);

    return (dispatch) => {
        dispatch({
            type: C.REG_TRY,
            payload: null
        });
        console.log('inside');

        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(user => registrationUserSuccess(dispatch, user, username, imageUri, gender))
            .catch(error => registrationUserFail(dispatch, error));
    };
};

const registrationUserFail = (dispatch, error) => {
    console.log('reg fail', error);
    dispatch({
        type: C.REG_ERROR,
        payload: error
    });
};

const registrationUserSuccess = (dispatch, user, username, imageUri, gender) => {
    if (imageUri) {
        let uploadPromise = uploadAvatarPromise(user.uid, imageUri);
        uploadPromise.then((url) => {
            finishRegistration(dispatch, user, username, url, gender);
        }).catch((err) => {
            dispatch({
                type: C.REG_ERROR,
                payload: err
            });
        })

    } else {
        finishRegistration(dispatch, user, username, null, gender);
    }
};

const finishRegistration = (dispatch, user, username, uploadedPhotoUrl, gender) => {
    let userRef = firebase.database().ref().child(C.F_USER_INFO + user.uid);
    //обновляется кастомный юзер
    userRef.set({
        displayName: username.toLowerCase(),
        photoURL: uploadedPhotoUrl,
        moderationCount: 0,
        gender
    });
    // обновляется юзер из Firebase
    user.updateProfile({
        displayName: username.toLowerCase(),
        photoURL: uploadedPhotoUrl
    }).then(() => {
        console.log('reg finished');
        console.log(user);
        dispatch({
            type: C.REG_SUCCESS,
            payload: user
        });
        Actions.main();
    }, error => dispatch({ type: C.REG_ERROR, payload: error }) );
};

