import C from './types';
import * as firebase from 'firebase';
import moment from 'moment';
require('moment/locale/ru'); //for import moment local language file during the application build
moment.locale('ru'); //set moment local language to zh-cn

import { Notification } from '../../models/Notification';
import { NOTIFICATION_TYPE, TIME_FORMAT } from '../../constants';
import { pushNotification } from './UserInfoActions';

export const fetchCommentsByPost = (postId, commentAmount) => {
    const commentsByPost = C.F_POST_COMMENTS + '/' + postId;
    const approvedPostsRef = firebase
        .database()
        .ref(commentsByPost)
        .limitToLast(commentAmount);
    return dispatch => {
        dispatch({ type: C.LOADING_COMMENTS_BY_POST, payload: postId });
        approvedPostsRef.on('value', snapshot => {
            dispatch({
                type: C.NEW_LOADED_COMMENT,
                payload: snapshot.val()
            });
        });
    };
};

export const fetchCommentsByUser = (userId, commentAmount) => {
    const commentsByUser = C.F_USER_COMMENTS + '/' + userId;
    const approvedPostsRef = firebase
        .database()
        .ref(commentsByUser)
        .limitToLast(commentAmount);
    return dispatch => {
        dispatch({ type: C.LOADING_COMMENTS_BY_USER, payload: userId });
        approvedPostsRef.on('value', snapshot => {
            dispatch({
                type: C.NEW_LOADED_COMMENT,
                payload: snapshot.val()
            });
        });
    };
};

export const postComment = comment => {
    return dispatch => {
        dispatch({
            type: C.COMMENT_SENT
        });
        let postTime = moment().format(TIME_FORMAT);

        const commentData = {
            postTime: postTime,
            userId: comment.userId,
            userImg: comment.userImg,
            body: comment.body,
            postId: comment.postId,
            userName: comment.userName
        };

        const newCommentKey = firebase
            .database()
            .ref()
            .child('comments')
            .push().key;
        let updates = {};
        updates[C.F_POST_COMMENTS + comment.postId + '/' + newCommentKey] = commentData;
        updates[C.F_USER_COMMENTS + comment.userId + '/' + newCommentKey] = commentData;
        firebase
            .database()
            .ref()
            .update(updates);

        firebase
            .database()
            .ref(`posts/posted/${comment.postId}`)
            .once('value')
            .then(function(snapshot) {
                const postShort = snapshot.val().body.substring(0, 140);
                const commentNotification = createPostCommentNotification(
                    comment.postId,
                    postShort,
                    comment.userId,
                    comment.userName,
                    comment.userImg,
                    comment.body,
                    newCommentKey,
                    postTime
                );
                pushNotification(snapshot.val().uid, commentNotification);

                comment.mentionedUsers.forEach(mentionedUser => {
                    const mentionNotification = createUserMentionNotification(
                        comment.postId,
                        postShort,
                        comment.body,
                        newCommentKey,
                        comment.userName,
                        comment.userId,
                        postTime
                    );
                    pushNotification(mentionedUser.uid, mentionNotification);
                });
            });
    };
};

const createUserMentionNotification = (postId, postShort, comment, commentId, authorName, authorUid, postTime) => {
    let notification = new Notification();
    notification.postId = postId;
    notification.postShort = postShort;
    notification.commentShort = comment.substring(0, 140);
    notification.commentId = commentId;
    notification.likeAuthorName = authorName;
    notification.likeAuthorUid = authorUid;
    notification.type = NOTIFICATION_TYPE.mention;
    notification.time = postTime;
    return notification;
};

const createPostCommentNotification = (
    postId,
    postShort,
    likeAuthorUid,
    likeAuthorName,
    likeAuthorPhotoURL,
    comment,
    commentId,
    postTime
) => {
    let notification = new Notification();
    notification.postId = postId;
    notification.postShort = postShort;
    notification.type = NOTIFICATION_TYPE.post_comment;
    notification.likeAuthorName = likeAuthorName;
    notification.likeAuthorPhotoURL = likeAuthorPhotoURL;
    notification.likeAuthorUid = likeAuthorUid;
    notification.time = postTime;
    notification.commentShort = comment.substring(0, 140);
    notification.commentId = commentId;
    return notification;
};

export const stopWatchingCommentsByPost = postId => {
    const commentsByPost = C.F_POST_COMMENTS + '/' + postId;
    return () => {
        firebase
            .database()
            .ref(commentsByPost)
            .off();
    };
};

export const stopWatchingCommentsByUser = userId => {
    const commentsByUser = C.F_USER_COMMENTS + '/' + userId;
    return () => {
        firebase
            .database()
            .ref(commentsByUser)
            .off();
    };
};

export const commentTyped = text => {
    return {
        type: C.COMMENT_TYPED,
        payload: text
    };
};
