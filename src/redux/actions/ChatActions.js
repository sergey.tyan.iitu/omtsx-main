import {Actions} from "react-native-router-flux";
import C from "./types";
import moment from 'moment';
require('moment/locale/ru'); //for import moment local language file during the application build
moment.locale('ru');//set moment local language to zh-cn
import * as firebase from 'firebase';
import { uploadChatImagePromise } from './photoUploadService';

export const fetchContacts = (myUid) => {
    const contactsRef = firebase.database().ref(C.F_USER_CHAT_CONTACTS + myUid);
    return (dispatch) => {
        dispatch({ type: C.LOAD_CONTACTS });
        contactsRef.on('value', snapshot => {
            dispatch({
                type: C.CONTACTS_LOADED,
                payload: snapshot.val()
            })
        });
    }
};

export const stopWatchingContacts = (myUid) => {
    return () => {
        firebase.database().ref(C.F_USER_CHAT_CONTACTS + myUid).off();
    };
};

export const fetchMessages = (myUid, contactUid, amount) => {
    const messagesRef = firebase.database().ref(C.F_USER_CHAT_MESSAGES + myUid + '/' + contactUid).limitToLast(amount);
    return (dispatch) => {
        // счётчик непрочитанных обнулить
        dispatch({ type: C.LOAD_MESSAGES });
        messagesRef.on('value', snapshot => {
            dispatch({
                type: C.MESSAGES_LOADED,
                payload: snapshot.val()
            })
        });
    }
};

export const stopWatchingMessages = (myUid, contactUid) => {
    return () => {
        firebase.database().ref(C.F_USER_CHAT_MESSAGES + myUid + '/' + contactUid).off();
    };
};

export const sendImage = (
    senderUid,
    senderName,
    senderImageUrl,
    recipientUid,
    recipientName,
    recipientImageUrl,
    imageUri
)=> {
    return (dispatch) => {
        dispatch({type:C.CHAT_IMG_UPLOADING});

        // получаем айдишник сообщения
        let messageKey = firebase.database().ref().child(C.F_USER_CHAT_MESSAGES + senderUid + '/' + recipientUid).push().key;
        let uploadPromise = uploadChatImagePromise(messageKey , imageUri);
        console.log('chat actions', imageUri);
        uploadPromise.then((url) => {
            dispatch({type:C.CHAT_IMG_SUCCESS});


            const createdAt = moment().format(C.TIME_FORMAT);
            const senderMsg = {
                image: url,
                createdAt:createdAt,
                uid: senderUid,
            };
            const recipientMsg = {
                image: url,
                createdAt:createdAt,
                uid: senderUid,
                imageUrl: senderImageUrl,
                name: senderName
            };
            const senderRecipientData = {
                uid: recipientUid,
                name: recipientName,
                imageUrl: recipientImageUrl,
                unreadCount: 0

            };

            const recipientSenderData = {
                uid: senderUid,
                name: senderName,
                imageUrl: senderImageUrl,
                lastUpdateDate: createdAt,
                lastMessage: '🏞 Image'
            };


            const r = C.F_USER_CHAT_MESSAGES + senderUid + '/' + recipientUid + '/' + messageKey;
            firebase.database().ref().update({
                [r]:senderMsg
            });
            //firebase.database().ref(C.F_USER_CHAT_MESSAGES + senderUid + '/' + recipientUid).push(senderMsg);
            firebase.database().ref(C.F_USER_CHAT_MESSAGES + recipientUid + '/' + senderUid).push(recipientMsg);

            firebase.database().ref(C.F_USER_CHAT_CONTACTS + senderUid + '/' + recipientUid).update(senderRecipientData);
            firebase.database().ref(C.F_USER_CHAT_CONTACTS + recipientUid + '/' + senderUid).update(recipientSenderData);

            // увеличение количества непрочитанных сообщений у собеседника
            firebase.database().ref(C.F_USER_CHAT_CONTACTS + recipientUid + '/' + senderUid + '/unreadCount').transaction((unreadCount) => {
                return unreadCount + 1;
            });

        }).catch((err) => {
            console.log(err);
            dispatch({
                type: C.CHAT_IMG_ERROR,
                payload: err
            });
        })
    }

};

export const sendMessage = (
    senderUid,
    senderName,
    senderImageUrl,
    recipientUid,
    recipientName,
    recipientImageUrl,
    message
) => {
    return () => {
        const createdAt = moment().format(C.TIME_FORMAT);
        const senderMsg = {
            text: message,
            createdAt:createdAt,
            uid: senderUid,
        };
        const recipientMsg = {
            text: message,
            createdAt:createdAt,
            uid: senderUid,
            imageUrl: senderImageUrl,
            name: senderName
        };
        const senderRecipientData = {
            uid: recipientUid,
            name: recipientName,
            imageUrl: recipientImageUrl,
            unreadCount: 0

        };

        const recipientSenderData = {
            uid: senderUid,
            name: senderName,
            imageUrl: senderImageUrl,
            lastUpdateDate: createdAt,
            lastMessage: message
        };

        firebase.database().ref(C.F_USER_CHAT_MESSAGES + senderUid + '/' + recipientUid).push(senderMsg);
        firebase.database().ref(C.F_USER_CHAT_MESSAGES + recipientUid + '/' + senderUid).push(recipientMsg);

        firebase.database().ref(C.F_USER_CHAT_CONTACTS + senderUid + '/' + recipientUid).update(senderRecipientData);
        firebase.database().ref(C.F_USER_CHAT_CONTACTS + recipientUid + '/' + senderUid).update(recipientSenderData);

        // увеличение количества непрочитанных сообщений у собеседника
        firebase.database().ref(C.F_USER_CHAT_CONTACTS + recipientUid + '/' + senderUid + '/unreadCount').transaction((unreadCount) => {
            return unreadCount + 1;
        });


    }
};
