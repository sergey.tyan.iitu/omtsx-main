import { StyleSheet } from 'react-native';

import { mainColor, mainColor2, width, height } from './constants';

module.exports = StyleSheet.create({
    spinnerStyle: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: mainColor
    },
    containerNews: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        backgroundColor: '#f2f2f2'
    },
    viewUser: {
        backgroundColor: '#f2f2f2'
    },
    center: {
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
        width: width
    },
    logo: {
        color: '#fff',
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 15
    },
    input: {
        height: 50,
        width: width - 30,
        backgroundColor: mainColor2,
        borderRadius: 6,
        color: '#fff',
        fontSize: 14,
        alignSelf: 'center',
        marginBottom: 15,
        paddingLeft: 20
    },
    reginput: {
        height: 50,
        width: width - 30,
        backgroundColor: mainColor2,
        borderRadius: 6,
        color: '#fff',
        fontSize: 14,
        marginBottom: 10,
        paddingLeft: 20,
        alignSelf: 'center'
    },
    button: {
        height: 50,
        width: width - 30,
        borderRadius: 6,
        justifyContent: 'center',
        alignSelf: 'stretch',
        marginBottom: 25,
        borderWidth: 2,
        borderStyle: 'solid',
        borderColor: mainColor2
    },

    width100: {
        height: 50,
        marginBottom: 15
    },

    regbutton: {
        height: 50,
        width: width - 30,
        borderRadius: 6,
        justifyContent: 'center',
        alignSelf: 'center',
        marginBottom: 25,
        borderWidth: 2,
        borderStyle: 'solid',
        borderColor: mainColor2
    },
    sogl: {
        fontSize: 14,
        color: '#dbd0ce',
        marginBottom: 2
    },
    usl: {
        fontSize: 14,
        color: '#dbd0ce',
        fontWeight: 'bold'
    },
    loginText: {
        color: '#9e8485',
        fontSize: 16,
        textAlign: 'center'
    },
    forgot: {
        fontSize: 16,
        color: '#dbd0ce',
        marginBottom: 8
    },
    forgotTitle: {
        fontSize: 20,
        color: '#fff',
        fontWeight: 'bold',
        marginBottom: 20
    },
    instruction: {
        fontSize: 14,
        color: '#dbd0ce',
        marginBottom: 8,
        textAlign: 'center',
        paddingRight: 15,
        paddingLeft: 15
    },
    helpLogin: {
        fontSize: 16,
        color: '#dbd0ce',
        fontWeight: 'bold'
    },
    footer: {
        height: 60,
        alignSelf: 'stretch',
        flexDirection: 'row',
        backgroundColor: '#6f3f3d',
        justifyContent: 'center',
        borderTopWidth: 2,
        borderTopColor: '#8c6464',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0
    },
    regText: {
        marginTop: 17,
        color: '#e3dddd',
        paddingRight: 5
    },
    regDo: {
        marginTop: 17,
        fontWeight: 'bold',
        color: '#e3dddd'
    },
    avatar: {
        marginBottom: 25,
        marginTop: -40,
        borderRadius: 70,
        borderWidth: 1,
        borderColor: '#fff'
    },
    avatarImg: {
        width: 120,
        height: 120,
        resizeMode: 'contain',
        borderRadius: 60,
        justifyContent: 'center',
        opacity: 0
    },

    avatarIcon: {
        position: 'absolute',
        left: 47,
        top: 37,
        backgroundColor: 'transparent'
    },
    wrapper: {
        width: width - 100
    },
    sliderLogo: {
        marginBottom: 30
    },
    sliderLogoImg: {
        width: 186,
        height: 186,
        resizeMode: 'contain',
        borderRadius: 93
    },
    sliderTitle: {
        fontSize: 16,
        fontWeight: 'bold',
        marginBottom: 10,
        color: '#fff'
    },
    sliderText: {
        color: '#fff',
        fontSize: 10,
        textAlign: 'center'
    },

    slide1: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    slide2: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    slide3: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    newsContentSingle: {
        marginTop: 10
    },
    tabContent: {
        backgroundColor: '#e8e8e8',
        marginTop: 10,
        width: width - 14,
        marginLeft: 7,
        height: 40,
        flexDirection: 'row',
        borderRadius: 3,
        marginBottom: 10
    },

    tabActive: {
        width: (width - 14) / 2,
        backgroundColor: '#fff',
        justifyContent: 'center',
        borderRadius: 3,
        borderWidth: 2,
        borderColor: '#e8e8e8',
        height: 40
    },
    tab: {
        width: (width - 14) / 2,
        backgroundColor: '#e8e8e8',
        justifyContent: 'center',
        borderRadius: 3,
        height: 40
    },

    tabText: {
        textAlign: 'center',
        fontSize: 11,
        fontWeight: 'bold',
        color: '#858585'
    },

    tabTextActive: {
        textAlign: 'center',
        fontSize: 11,
        fontWeight: 'bold',
        color: '#313131'
    },

    listS: {
        flexDirection: 'column',
        width: width,
        marginTop: 10
    },
    listNwrap: {
        borderRadius: 5
    },
    listProfileHeader: {
        marginBottom: 10
    },
    headItem: {
        flexDirection: 'row',
        flexBasis: width - 14,
        height: 60,
        flexGrow: 1
    },
    listProfile: {
        marginTop: 10
    },
    item: {
        backgroundColor: '#fff',
        borderRadius: 6,
        borderWidth: 1,
        borderColor: '#e8e8e8',
        marginBottom: 15,
        marginLeft: 7,
        width: width - 14,
        paddingTop: 15
    },
    itemProfileNews: {
        backgroundColor: '#fff',
        borderRadius: 6,
        borderWidth: 1,
        borderColor: '#e8e8e8',
        marginBottom: 15,
        width: width - 14,
        marginLeft: 7
    },

    imgItem: {
        width: 45,
        height: 45,
        backgroundColor: '#e9e9e9',
        borderRadius: 37,
        margin: 9
    },

    imgProfile: {
        width: 35,
        height: 35,
        backgroundColor: '#e9e9e9',
        borderRadius: 37
    },

    imgProfileN: {
        width: 60,
        height: 60,
        backgroundColor: '#e9e9e9',
        borderRadius: 37,
        marginRight: 15
    },
    imgProfileSub: {
        width: 45,
        height: 45,
        backgroundColor: '#e9e9e9',
        borderRadius: 23,
        marginLeft: 10,
        marginTop: 7
    },
    nAvatar: {
        height: 60
    },
    subAvatar: {
        height: 40
    },
    itemImage: {
        resizeMode: 'contain',
        width: 45,
        height: 45,
        borderRadius: 22
    },
    itemImageSub: {
        resizeMode: 'contain',
        width: 45,
        height: 45,
        borderRadius: 22
    },
    itemImageN: {
        resizeMode: 'contain',
        width: 60,
        height: 60,
        borderRadius: 30
    },

    titleItem: {
        color: '#6b3a3b',
        fontSize: 16,
        fontWeight: 'bold'
    },
    catItem: {
        color: '#b9bfc4',
        fontSize: 13
    },
    titleItemWr: {
        justifyContent: 'center'
    },
    contextModal: {
        position: 'absolute',
        right: 18,
        top: 6
    },

    itemMainImage: {
        height: 250,
        width: width - 16,
        resizeMode: 'contain'
    },

    titleContent: {
        paddingTop: 10,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 15,
        color: '#343434',
        fontWeight: 'bold'
    },
    textContent: {
        paddingTop: 3,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 13,
        color: '#171717',
        marginBottom: 25
    },
    readMore: {
        paddingTop: 3,
        paddingLeft: 10,
        paddingRight: 10,
        fontSize: 11,
        color: '#4f1a18',
        paddingBottom: 7
    },
    soc: {
        marginTop: 10,
        height: 33,
        flexDirection: 'row'
    },
    likesIcon: {
        marginLeft: 8
    },
    likesCount: {
        marginRight: 5,
        color: '#959596',
        fontSize: 14,
        marginTop: 2,
        marginLeft: 5
    },
    commentsIcon: {
        marginLeft: 5
    },
    commentsCount: {
        marginLeft: 5,
        color: '#959596',
        fontSize: 14,
        marginTop: 2
    },
    nothing: {
        height: 0,
        position: 'absolute',
        left: -9999
    },

    itemLink: {
        height: 40,
        backgroundColor: mainColor,
        justifyContent: 'center'
    },
    textLink: {
        marginLeft: 10,
        color: '#fff',
        fontSize: 13
    },
    rightArrow: {
        position: 'absolute',
        right: 12,
        top: 11
    },
    nextArrow: {
        position: 'absolute',
        right: 12,
        top: 9,
        zIndex: 99,
        backgroundColor: 'transparent'
    },
    category: {
        color: '#959596',
        fontSize: 13,
        marginTop: 2,
        position: 'absolute',
        right: 12
    },
    selectText: {
        color: mainColor
    },
    modalSection: {
        height: 0
    },
    cancelText: {
        color: mainColor,
        fontWeight: 'bold'
    },
    selectStyle: {
        flex: 1,
        borderColor: '#ccc',
        borderWidth: 1,
        padding: 0,
        borderRadius: 5
    },
    navList: {
        flexDirection: 'row',
        height: 64,
        backgroundColor: mainColor2,

        justifyContent: 'space-around'
    },
    accountInfoOne: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        backgroundColor: 'rgba(0,0,0,0.5)',
        justifyContent: 'center',
        alignItems: 'center'
    },
    navTitleText: {
        color: '#fff',
        textAlign: 'center'
    },
    navTitleTextBold: {
        color: '#fff',
        textAlign: 'left',
        fontSize: 16,
        fontWeight: '600'
    },
    leftNav: {
        flexGrow: 1,
        justifyContent: 'center'
    },
    rightNav: {
        flexGrow: 1,
        justifyContent: 'center'
    },
    iconLeft: {
        position: 'absolute',
        left: 13,
        top: 25,
        color: '#fff'
    },
    iconRight: {
        alignSelf: 'flex-end',
        position: 'absolute',
        right: 13,
        top: 25,
        width: 26,
        color: '#fff'
    },
    leftMenu: {
        backgroundColor: '#fff',
        flex: 1
    },
    leftSearch: {
        backgroundColor: mainColor,
        flex: 1,
        width: width
    },
    menuOut: {
        height: 999,
        opacity: 0.2,
        backgroundColor: '#000',
        position: 'absolute',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        zIndex: 99
    },
    searchInput: {
        backgroundColor: '#e9e9e9',
        borderRadius: 5,
        marginLeft: 15,
        paddingLeft: 30,
        marginTop: 15,
        height: 35,
        fontSize: 12,
        color: '#bca6a8',
        width: width - 95
    },
    searchIcon: {
        position: 'absolute',
        left: 25,
        top: 23,
        width: 16,
        height: 16,
        backgroundColor: 'transparent'
    },
    menuItems: {
        marginTop: 15,
        marginLeft: 15
    },
    avatarProfile: {
        flexDirection: 'row',
        marginBottom: 15
    },
    nameProfile: {
        justifyContent: 'center',
        marginLeft: 10
    },
    nameProfileText: {
        fontWeight: '500'
    },
    menuItem: {
        flexDirection: 'row',

        width: width - 90,
        marginBottom: 13
    },
    menuIcon: {
        marginLeft: 9,
        marginRight: 17,
        marginTop: -5
    },
    textItem: {
        justifyContent: 'center'
    },

    textItemActive: {
        color: '#5b080a'
    },
    notificationCount: {
        position: 'absolute',
        right: 5,
        top: 2,
        backgroundColor: mainColor,
        height: 17,
        width: 17,
        borderRadius: 15,
        justifyContent: 'center'
    },
    nCountText: {
        textAlign: 'center',
        color: '#fff',
        fontSize: 8,
        backgroundColor: 'transparent'
    },
    searchInputActive: {
        backgroundColor: '#734545',
        borderRadius: 5,
        flexGrow: 1,
        marginLeft: 15,
        paddingLeft: 30,
        marginTop: 15,
        height: 35,
        fontSize: 12,
        color: '#bca6a8'
    },
    textCancel: {
        color: '#fff',
        backgroundColor: 'transparent',
        textAlign: 'center',
        marginTop: 12
    },

    cancelBtn: {
        justifyContent: 'center',
        flexGrow: 1,
        opacity: 0
    },
    searchBar: {
        width: width - 75
    },
    searchBox: {
        flexDirection: 'row'
    },
    searchRes: {
        paddingRight: 15
    },
    searchItem: {
        backgroundColor: 'transparent',
        borderBottomWidth: 1,
        borderBottomColor: '#764848',
        marginLeft: 15,
        marginTop: 15,
        height: 30
    },
    searchItemText: {
        fontSize: 15,
        color: '#fff'
    },
    bottomBtn: {
        position: 'absolute',
        bottom: 10,
        flexDirection: 'row',
        backgroundColor: mainColor,
        borderRadius: 5,
        width: width - 14,
        marginLeft: 7,
        height: 40,
        justifyContent: 'center'
    },
    nextBtn: {},
    nextText: {
        textAlign: 'center',
        color: '#fff',
        marginTop: 9
    },
    itemN: {
        backgroundColor: '#fff',
        padding: 12,
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#eaeaea',
        width: width,
        // width: width - 14,
        overflow: 'hidden'
    },
    itemNComments: {
        backgroundColor: '#fff',
        flexDirection: 'column',
        borderBottomWidth: 1,
        borderBottomColor: '#eaeaea',
        width: width - 14,
        overflow: 'hidden'
    },
    contentComment: {
        flexDirection: 'row',
        padding: 12
    },
    listN: {
        paddingTop: 5
    },
    smallIcon: {},
    transparent: {
        color: 'white',
        backgroundColor: 'transparent',
        marginTop: 2,
        textAlign: 'center'
    },
    nDesc: {},

    headN: {
        flexDirection: 'row',
        marginTop: 3,
        width: width - 100
    },
    footN: {
        flexDirection: 'row',
        width: width - 100
    },
    footNCom: {
        marginTop: 5,
        flexDirection: 'row',
        width: width - 100,
        lineHeight: 18
    },
    targetComment: {
        color: mainColor,
        marginTop: 3
    },
    nameItemN: {
        color: mainColor,
        fontWeight: 'bold'
    },
    timeItemN: {
        color: '#c1c6cb',
        marginTop: 3
    },
    likes: {
        color: '#c1c6cb',
        marginTop: 3
    },
    targetItemN: {
        color: mainColor,
        width: width - 100
    },
    target: {
        marginTop: 4
    },
    nPhoto: {
        width: 53,
        height: 53,
        backgroundColor: '#e9e9e9',
        right: 11,
        top: 15,
        position: 'absolute'
    },
    photoItemN: {
        width: 53,
        height: 53,
        resizeMode: 'contain'
    },
    txtCommentItemN: {
        width: width - 120
    },
    countNews: {
        backgroundColor: '#f2f2f2',
        justifyContent: 'center',
        height: 73,
        width: width
    },
    countNewsText: {
        textAlign: 'center',
        color: '#78889d'
    },
    textLeft: {
        marginTop: 20,
        fontSize: 15,
        color: '#fff',
        paddingLeft: 15
    },
    textRight: {
        fontSize: 15,
        color: '#fff',
        marginTop: 20,
        textAlign: 'right',

        paddingRight: 15
    },
    navTitleMain: {
        flexGrow: 1,
        justifyContent: 'center',
        marginTop: 20
    },
    navTitle: {
        flexGrow: 1,
        justifyContent: 'center',
        marginTop: 20
    },
    section: {
        backgroundColor: '#fff',
        marginTop: 10,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#eeeeee',
        width: width - 14
    },
    sectionAgree: {
        backgroundColor: '#fff',
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#eeeeee',
        width: width - 14,
        padding: 10,
        alignSelf: 'center',
        marginBottom: 10
    },
    agreeText: {
        color: '#585858',
        fontWeight: '200'
    },
    sectionItem: {
        flexDirection: 'row',
        height: 45,
        borderBottomWidth: 1,
        borderBottomColor: '#ecebec'
    },
    postBodyContainer: {
        flexDirection: 'row',
        height: 335,
        borderBottomWidth: 1,
        borderBottomColor: '#ecebec'
    },
    sectionItemCenter: {
        flexDirection: 'row',
        height: 45,
        borderBottomColor: '#ecebec',
        alignItems: 'center',
        justifyContent: 'center'
    },
    imgEdit1: {
        width: 35,
        height: 35
    },
    imgEdit1Img: {
        width: 35,
        height: 35,
        borderRadius: 17,
        resizeMode: 'contain',
        marginLeft: 20,
        marginTop: 4
    },
    imgEdit2: {
        width: 35,
        height: 35
    },
    imgEdit2Img: {
        width: 35,
        height: 35,
        borderRadius: 3,
        resizeMode: 'contain',
        marginLeft: 20,
        marginTop: 4
    },
    basicEditText: {
        marginLeft: 35,
        fontSize: 16,
        color: '#697d96',
        marginTop: 11,
        fontWeight: '200'
    },
    label: {
        flexGrow: 1,
        justifyContent: 'center'
    },
    textLabel: {
        fontSize: 16,
        marginLeft: 20,
        fontWeight: '200'
    },
    textInput: {
        flexGrow: 3,
        justifyContent: 'center'
    },
    textInputFull: {
        justifyContent: 'center',
        marginRight: 20,
        flexGrow: 1
    },
    textInputFullLeft: {
        justifyContent: 'center',
        marginLeft: 20,
        flexGrow: 1
    },
    profileInput: {
        height: 50,
        textAlign: 'right',
        marginRight: 20,
        fontWeight: '200',
        color: '#697d96',
        fontSize: 15
    },
    profileInputFull: {
        height: 30,
        marginLeft: 20,
        fontSize: 14,
        flexGrow: 1,
        fontStyle: 'italic',
        fontWeight: '200',
        color: '#697d96'
    },
    profileInputFullLeft: {
        height: 30,
        fontSize: 16,
        flexGrow: 1,
        fontWeight: '200',
        color: '#697d96'
    },
    categoryInput: {
        height: 30,
        fontSize: 16,
        flexGrow: 1,
        fontWeight: '200',
        color: '#697d96',
        marginTop: 12
    },
    postBody: {
        height: 320,
        flexGrow: 1,
        marginLeft: 20,
        marginRight: 20,
        marginTop: 10,
        marginBottom: 10
    },

    moder: {
        width: width - 14,
        height: 40,
        justifyContent: 'center'
    },
    moderText: {
        alignSelf: 'center',
        fontSize: 13,
        color: '#697d96'
    },
    commentBtn: {
        position: 'absolute',
        left: 0,
        bottom: 0,
        top: 40,
        right: 0,
        zIndex: 999
    },
    actionsComment: {
        backgroundColor: '#f2f2f2',
        flexDirection: 'row'
    },
    cAction: {
        flexGrow: 1,
        justifyContent: 'center',
        height: 40
    },
    contextIcon: {
        textAlign: 'center'
    },
    containerCat: {
        marginTop: 60
    },
    containerCats: {
        width: width - 14,
        backgroundColor: '#fff',
        borderRadius: 5,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#e8e8e8',
        marginBottom: 10
    },
    listCat: {
        width: width - 7
    },
    containerSubcribers: {
        width: width - 14,
        backgroundColor: '#fff',
        borderRadius: 5,
        marginLeft: 7,
        marginTop: 10,
        borderWidth: 1,
        borderColor: '#e8e8e8'
    },
    itemCat: {
        flexDirection: 'row',
        width: width - 30,
        height: 45,
        borderBottomWidth: 1,
        borderBottomColor: '#eaeaea',
        alignItems: 'center',
        justifyContent: 'space-between',
        paddingRight: 10
    },
    itemFaq: {
        backgroundColor: '#fff',
        overflow: 'hidden',
        borderRadius: 5,
        width: width - 16
    },
    borderBottomFuck: {
        flex: 1,
        height: 1,
        backgroundColor: '#eaeaea'
    },
    answer: {
        marginLeft: 15,
        marginTop: 10,
        marginBottom: 15,
        fontSize: 12,
        color: '#494949',
        paddingRight: 15
    },
    itemSub: {
        flexDirection: 'row',
        height: 60,
        borderBottomWidth: 1,
        borderBottomColor: '#eaeaea'
    },
    catName: {
        color: '#79899e',
        fontWeight: '200'
    },
    faqName: {
        marginTop: 13,
        marginBottom: 13,
        marginLeft: 15,
        color: '#79899e',
        fontWeight: '200'
    },
    catIcon: {
        position: 'absolute',
        right: 20,
        top: 13
    },
    userMainImage: {
        width: width,
        height: 215
    },
    userBg: {
        width: width,
        height: 215
    },
    accountInfo: {
        justifyContent: 'center',
        alignItems: 'center'
    },
    editButton: {
        flexDirection: 'row',
        backgroundColor: '#fff',
        width: 120,
        height: 25,
        borderRadius: 3,
        justifyContent: 'center',
        marginTop: 20
    },
    editButtonText: {
        backgroundColor: 'transparent',
        fontSize: 10,
        marginTop: 6,
        color: '#7889a0'
    },
    editIcon: {
        backgroundColor: 'transparent',
        marginRight: 6,
        alignSelf: 'center'
    },
    accountAvatar: {
        borderRadius: 60,
        borderWidth: 1,
        borderColor: '#fff',
        marginTop: 10,
        backgroundColor: '#fff'
    },
    accountNick: {
        marginTop: 10
    },
    accountNickText: {
        fontSize: 10,
        fontWeight: 'bold',
        color: '#fff'
    },
    accountStatus: {
        marginTop: 10
    },
    accountStatusText: {
        fontSize: 10,
        fontStyle: 'italic',
        color: '#fff'
    },
    accountAvatarImage: {
        width: 100,
        height: 100,
        resizeMode: 'contain',
        borderRadius: 50
    },
    profileCounts: {
        height: 35,
        borderBottomWidth: 1,
        borderBottomColor: '#eaeaea',
        width: width,
        backgroundColor: '#fff',
        flexDirection: 'row'
    },
    profileCount: {
        justifyContent: 'center',
        alignItems: 'center',
        flexGrow: 1
    },
    profileCountText: {
        fontSize: 11,
        fontWeight: '500',
        color: '#000'
    },
    itemsProfile: {
        width: width - 14,
        marginLeft: 7,
        borderRadius: 5,
        backgroundColor: '#fff',
        borderWidth: 1,
        borderColor: '#e8e8e8',
        marginTop: 10,
        borderBottomWidth: 0
    },
    itemProfile: {
        flexDirection: 'row',
        backgroundColor: 'transparent',
        height: 40,
        borderBottomWidth: 1,
        borderBottomColor: '#eaeaea'
    },
    itemProfileLeft: {
        flexGrow: 1,
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    itemProfileCenter: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemProfileRight: {
        flexGrow: 1,
        alignItems: 'flex-end',
        justifyContent: 'center'
    },
    itemProfileLeftIcon: {
        marginLeft: 15
    },
    itemProfileRightText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: 11,
        backgroundColor: 'transparent'
    },
    itemProfileCenterText: {
        fontSize: 12,
        fontWeight: '600',
        color: '#8c98ab'
    },
    itemProfileRightWrap: {
        width: 23,
        height: 14,
        borderRadius: 6,
        backgroundColor: '#ccc',
        marginRight: 15,
        alignItems: 'center',
        justifyContent: 'center'
    },
    itemAllCounts: {
        height: 40,
        alignItems: 'center',
        width: width,
        justifyContent: 'center'
    },
    itemAllCountsText: {
        fontSize: 11,
        color: '#8c98ab'
    },
    sectionLabel: {
        marginTop: 20
    },
    sectionLabelText: {
        color: '#79899e',
        fontSize: 12,
        textAlign: 'left',
        width: width - 24
    },
    sectionLabelBottom: {
        marginTop: 5,
        marginBottom: 15
    },
    sectionLabelSettings: {
        marginTop: 65
    },
    sectionLabelFirst: {
        marginTop: 20
    },
    exit: {
        color: '#a01300'
    },
    subscriberInfo: {
        marginLeft: 10,
        justifyContent: 'center'
    },
    btnSub: {
        position: 'absolute',
        right: 25,
        top: 15,
        borderWidth: 1,
        borderColor: '#babfc3',
        width: 100,
        height: 28,
        borderRadius: 3,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    subIcon: {
        width: 20
    },
    subText: {
        fontSize: 10,
        color: '#12121c'
    },
    commentInput: {
        color: 'black',
        fontSize: 14,
        height: 30,
        padding: 5,
        marginLeft: 5,
        backgroundColor: 'white',
        flex: 4,
        borderRadius: 6,
        alignSelf: 'center'
    },
    commentInputContainer: {
        borderRadius: 6,
        backgroundColor: mainColor2,
        alignItems: 'center',
        justifyContent: 'center',
        margin: 10,
        height: 40,
        flexDirection: 'row'
    },
    sendCommentButtonContainer: {
        flex: 1,
        backgroundColor: mainColor2,
        height: 30,
        alignItems: 'center'
    },
    sendCommentButton: {
        justifyContent: 'center',
        height: 30
    },
    sendCommentButtonText: {
        textAlign: 'center',
        fontSize: 14,
        fontWeight: 'bold',
        color: 'white'
    },
    loadingOverlay: {
        flex: 1,
        position: 'absolute',
        left: 0,
        top: 0,
        opacity: 0.5,
        backgroundColor: 'black',
        height: height,
        width: width
    },
    uploadedImage: {
        width: width / 4,
        height: width / 4,
        margin: 10,
        marginTop: height / 10
    },
    accountModal: {
        flex: 1,
        backgroundColor: 'rgba(255, 255, 255, 0.8)'
    },
    modalContainer: {
        flex: 1,
        margin: 10,
        justifyContent: 'flex-start'
    },
    modalButtonContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },

    navTest: {
        paddingTop: 20,
        height: 64,
        backgroundColor: mainColor2,
        borderBottomWidth: StyleSheet.hairlineWidth,
        flexDirection: 'row', // step 1
        justifyContent: 'space-between', // step 2
        alignItems: 'center' // step 3
    },
    navItem: {
        width: width / 3
    },
    iconLeftTest: {
        color: '#fff',
        left: 15
    },
    navTitleTextTest: {
        color: '#fff',
        textAlign: 'center'
    },
    iconRightTest: {
        alignSelf: 'flex-end',
        color: '#fff',
        right: 15
    }
});
