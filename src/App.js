import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import reducers from './redux/reducers';
import Router from './Router';

import { createLogger } from 'redux-logger';

const logger = createLogger({
    // ...options
    collapsed: true
});

class App extends Component {
    componentWillMount() {
        const config = {
            apiKey: 'AIzaSyDBs9Kv1ctS_XU0m-EXc6GYmsJUQ_PPs60',
            authDomain: 'blinding-heat-7224.firebaseapp.com',
            databaseURL: 'https://blinding-heat-7224.firebaseio.com',
            storageBucket: 'blinding-heat-7224.appspot.com',
            messagingSenderId: '906869099578'
        };

        firebase.initializeApp(config);
    }

    render() {
        console.ignoredYellowBox = ['Remote debugger'];
        const store = createStore(reducers, {}, applyMiddleware(ReduxThunk, logger));

        if (module.hot) {
            module.hot.accept(() => {
                const nextRootReducer = require('./redux/reducers/index').default;
                store.replaceReducer(nextRootReducer);
            });
        }

        return (
            <Provider store={store}>
                <Router />
            </Provider>
        );
    }
}

export default App;
