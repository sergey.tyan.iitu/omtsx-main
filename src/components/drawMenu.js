/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { StatusBar, Platform } from 'react-native';
import Drawer from 'react-native-drawer';
import SideBarMenu from './sideMenu';

import styles from '../style';

export default class omtsx extends Component {
    constructor(props) {
        super(props);

        this.state = {
            drawerType: 'static',
            openDrawerOffset: 60,
            closedDrawerOffset: 0,
            panOpenMask: 0.1,
            panCloseMask: 0.15,
            relativeDrag: false,
            panThreshold: 0.25,
            tweenHandlerOn: false,
            tweenDuration: 350,
            tweenEasing: 'linear',
            disabled: false,
            tweenHandlerPreset: null,
            acceptDoubleTap: false,
            acceptTap: false,
            acceptPan: false,
            tapToClose: true,
            negotiatePan: true,
            rightSide: true
        };
    }

    closeDrawer() {
        this.drawer.close();
    }

    onSearch() {
        if (Platform.OS !== 'android') {
            StatusBar.setHidden(true);
        }
        this.setState(
            {
                openDrawerOffset: 0,
                panCloseMask: 0
            },
            function() {
                this.drawer.open();
            }
        );
    }

    outSearch() {
        this.setState(
            {
                openDrawerOffset: 60,
                panCloseMask: 0.15
            },
            () => {
                this.drawer.open();
                if (Platform.OS !== 'android') {
                    StatusBar.setHidden(false);
                }
            }
        );
    }

    openDrawer() {
        this.drawer.open();
    }

    render() {
        const sideMenu = (
            <SideBarMenu
                closeDrawer={() => {
                    this.drawer.close();
                }}
                onSearch={this.onSearch.bind(this)}
                outSearch={this.outSearch.bind(this)}
            />
        );

        const Component = this.props.component;

        return (
            <Drawer
                ref={ref => {
                    this.drawer = ref;
                }}
                content={sideMenu}
                styles={drawerStyles}
                onCloseStart={this.stopMonitoring}
                type={this.state.drawerType}
                animation={this.state.animation}
                openDrawerOffset={this.state.openDrawerOffset}
                closedDrawerOffset={this.state.closedDrawerOffset}
                panOpenMask={this.state.panOpenMask}
                panCloseMask={this.state.panCloseMask}
                relativeDrag={this.state.relativeDrag}
                panThreshold={this.state.panThreshold}
                disabled={this.state.disabled}
                acceptDoubleTap={this.state.acceptDoubleTap}
                acceptTap={this.state.acceptTap}
                acceptPan={this.state.acceptPan}
                tapToClose={this.state.tapToClose}
                negotiatePan={this.state.negotiatePan}
                changeVal={this.state.changeVal}
                side={'left'}
                tweenHandler={ratio => ({
                    mainOverlay: {
                        opacity: ratio / 1.5,
                        backgroundColor: 'black'
                    }
                })}
            >
                <Component
                    logOut={this.props.logOut}
                    style={styles.menuOut}
                    title={this.props.title}
                    drawerType={this.state.drawerType}
                    openDrawer={this.openDrawer.bind(this)}
                    openDrawerOffset={this.state.openDrawerOffset}
                    closedDrawerOffset={this.state.closedDrawerOffset}
                    panOpenMask={this.state.panOpenMask}
                    panCloseMask={this.state.panCloseMask}
                    relativeDrag={this.state.relativeDrag}
                    panStartCompensation={this.state.panStartCompensation}
                    tweenHandlerOn={this.state.tweenHandlerOn}
                    disabled={this.state.disabled}
                    panThreshold={this.state.panThreshold}
                    tweenEasing={this.state.tweenEasing}
                    tweenHandlerPreset={this.state.tweenHandlerPreset}
                    acceptTap={this.state.acceptTap}
                    acceptDoubleTap={this.state.acceptDoubleTap}
                    acceptPan={this.state.acceptPan}
                    tapToClose={this.state.tapToClose}
                    negotiatePan={this.state.negotiatePan}
                    rightSide={this.state.rightSide}
                />
            </Drawer>
        );
    }
}

const drawerStyles = {
    drawer: {
        shadowColor: '#000000',
        shadowOpacity: 0,
        shadowRadius: 0,
        width: 999
    },
    main: { backgroundColor: '#000' }
};
