import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableHighlight,
    TouchableNativeFeedback,
    Platform
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import styles from '../style.js';

const TouchableElement =
    Platform.OS === 'android' ? TouchableNativeFeedback : TouchableHighlight;

export default class ControlPanel extends Component {
    render() {
        return (
            <View style={styles.navTest}>
                <View style={styles.navItem}>
                    <TouchableElement
                        underlayColor="transparent"
                        onPress={this.props.openDrawer}
                    >
                        <Icon
                            style={styles.iconLeftTest}
                            name={'md-menu'}
                            size={26}
                        />
                    </TouchableElement>
                </View>

                <View style={styles.navItem}>
                    <Text style={styles.navTitleTextTest}>
                        {this.props.title}
                    </Text>
                </View>

                <View style={styles.navItem}>
                    {this.props.rightButtonAction && (
                        <TouchableElement
                            underlayColor="transparent"
                            onPress={this.props.rightButtonAction}
                        >
                            <Icon
                                name={'md-add'}
                                style={styles.iconRightTest}
                                size={26}
                            />
                        </TouchableElement>
                    )}
                </View>
            </View>
        );
    }
}
