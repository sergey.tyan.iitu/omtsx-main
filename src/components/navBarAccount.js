import React, { Component } from 'react';
import {
    View,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    Platform
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';
import styles from '../style.js';

const TouchableElement =
    Platform.OS === 'android' ? TouchableOpacity : TouchableHighlight;

export default class ControlPanel extends Component {
    leftButton() {
        if (this.props.goBack) {
            if (Platform.OS === 'ios') {
                return (
                    <View style={styles.navItem}>
                        <TouchableElement
                            underlayColor="transparent"
                            onPress={() => Actions.pop()}
                        >
                            <Icon
                                style={styles.iconLeftTest}
                                name={'md-arrow-round-back'}
                                size={26}
                            />
                        </TouchableElement>
                    </View>
                );
            } else {
                return <View style={styles.navItem} />;
            }
        } else {
            return (
                <View style={styles.navItem}>
                    <TouchableElement
                        underlayColor="transparent"
                        onPress={this.props.openDrawer}
                    >
                        <Icon
                            style={styles.iconLeftTest}
                            name={'md-menu'}
                            size={26}
                        />
                    </TouchableElement>
                </View>
            );
        }
    }

    rightIcon() {
        if (this.props.rightIcon) {
            return (
                <View style={styles.navItem}>
                    <TouchableElement
                        underlayColor="transparent"
                        onPress={this.props.rightIconPress}
                    >
                        <Icon
                            name={
                                this.props.rightIcon !== ''
                                    ? this.props.rightIcon
                                    : 'md-menu'
                            }
                            style={styles.iconRightTest}
                            size={this.props.rightIcon !== '' ? 26 : 0.01}
                        />
                    </TouchableElement>
                </View>
            );
        } else {
            return <View style={styles.navItem} />;
        }
    }

    render() {
        return (
            <View style={styles.navTest}>
                {this.leftButton()}
                <View style={styles.navItem}>
                    <Text style={styles.navTitleTextTest}>
                        {this.props.title}
                    </Text>
                </View>
                {this.rightIcon()}
            </View>
        );
    }
}
