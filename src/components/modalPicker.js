import React, { Component } from 'react';
import { View, Text, Dimensions, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

import ModalPicker from 'react-native-modal-picker';
var statusheight;
if (Platform.OS === 'android') {
    statusheight = 25;
}
var width = Dimensions.get('window').width;
var height = Dimensions.get('window').height;
var styles = require('../style.js');
let index = 0;
const actions = [
    { key: index++, label: 'Какое-то действие' },
    { key: index++, label: 'Какое-то действие' },
    { key: index++, label: 'Какое-то действие' }
];

const stylesInner = require('react-native').StyleSheet.create({
    optionContainer: {
        top: height - 40 * actions.length - 55 - statusheight,
        borderRadius: 5,
        width: width - 14,
        backgroundColor: 'rgba(255,255,255,0.9)',
        height: 40 * actions.length,
        overflow: 'hidden',
        left: 7
    },
    option: {
        height: 40
    },

    cancelContainer: {
        left: 7,
        top: 40 * actions.length + 5,
        position: 'absolute',
        height: 50
    },

    allCont: {
        top: -50,
        alignSelf: 'flex-end'
    },

    overlay: {
        flexDirection: 'row'
    },
    cancel: {}
});

export default class ControlPanel extends Component {
    render() {
        return (
            <ModalPicker
                style={styles.contextModal}
                data={actions}
                cancelContainer={stylesInner.cancelContainer}
                initValue="Select something yummy!"
                optionStyle={stylesInner.option}
                optionTextStyle={styles.selectText}
                allCont={stylesInner.allCont}
                overlayStyle={stylesInner.overlay}
                optionContainer={stylesInner.optionContainer}
                sectionStyle={styles.modalSection}
                selectStyle={styles.selectStyle}
                cancelTextStyle={styles.cancelText}
                onChange={option => {
                    this.setState({ textInputValue: option.label });
                }}
            >
                <Icon
                    name={'ios-more'}
                    color="#babfc3"
                    style={styles.contextIcon}
                    size={32}
                />
            </ModalPicker>
        );
    }
}
