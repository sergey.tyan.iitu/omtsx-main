import React from 'react';
import { Scene, Router } from 'react-native-router-flux';
import LoginPage from './routes/login';
import SignUpPage from './routes/registration';
import NewsPage from './routes/news';
import BestPage from './routes/best';
import ForgotPage from './routes/forgot';
import RandomPage from './routes/random';
import NotificationsPage from './routes/notifications';
import CategoriesPage from './routes/categories';
import AgreementPage from './routes/agreement';
import CatSelect from './routes/catSelect';
import SettingsPage from './routes/settings';
import Account from './routes/account';
import FaqPage from './routes/faq';
import Slider from './routes/slider';
import CommentsPage from './routes/comments';
import NewPost from './routes/new';
import CategoryPosts from './routes/CategoryPosts';
import Messages from './routes/Messages';
import ChatContacts from './routes/ChatContacts';
import AccountPage from './routes/include/account';
import LikedList from './routes/LikedList';
import SinglePost from './routes/include/SinglePost';
import UserPostsList from './routes/include/userPostsList';
import UserLikedList from './routes/include/userLikedList';
import UserModeratedList from './routes/include/userModeratedList';

const RouterComponent = () => {
    return (
        <Router>
            <Scene key="auth" style={{ marginTop: 0 }}>
                <Scene key="login" component={LoginPage} hideNavBar={true} />
                <Scene key="signup" component={SignUpPage} hideNavBar={true} />
                <Scene key="forgot" component={ForgotPage} hideNavBar={true} />

                <Scene key="slider" component={Slider} panHandlers={null} title="Slider" hideNavBar={true} />
                <Scene
                    key="agreement2"
                    component={AgreementPage}
                    panHandlers={null}
                    title="Пользовательское соглашение"
                    hideNavBar={true}
                />
            </Scene>

            <Scene key="main" style={{ marginTop: 0 }}>
                <Scene key="news" component={NewsPage} title="OMTSX" hideNavBar={true} panHandlers={null} />
                <Scene key="best" component={BestPage} title="OMTSX" hideNavBar={true} panHandlers={null} />
                <Scene key="random" component={RandomPage} title="Случайные" hideNavBar={true} panHandlers={null} />
                <Scene
                    key="notifications"
                    component={NotificationsPage}
                    title="Уведомления"
                    hideNavBar={true}
                    panHandlers={null}
                />
                <Scene
                    key="categories"
                    component={CategoriesPage}
                    title="Категории"
                    hideNavBar={true}
                    panHandlers={null}
                />
                <Scene key="settings" component={SettingsPage} title="Настройки" panHandlers={null} />
                <Scene key="account" component={Account} panHandlers={null} title="Аккаунт" hideNavBar={true} />
                <Scene
                    key="agreement"
                    component={AgreementPage}
                    panHandlers={null}
                    title="Пользовательское соглашение"
                    hideNavBar={true}
                />
                <Scene
                    key="catSelect"
                    component={CatSelect}
                    panHandlers={null}
                    title="Пользовательское соглашение"
                    hideNavBar={true}
                />
                <Scene key="faq" component={FaqPage} panHandlers={null} title="FAQ" hideNavBar={true} />
                <Scene
                    key="comments"
                    component={CommentsPage}
                    panHandlers={null}
                    title="Комментарии"
                    hideNavBar={true}
                />
                <Scene
                    key="newPost"
                    component={NewPost}
                    panHandlers={null}
                    title="Написать историю"
                    hideNavBar={true}
                />
                <Scene key="categoryPost" component={CategoryPosts} panHandlers={null} hideNavBar={true} />
                <Scene key="messages" component={Messages} panHandlers={null} hideNavBar={true} />
                <Scene key="contacts" component={ChatContacts} panHandlers={null} hideNavBar={true} />
                <Scene key="accountPage" component={AccountPage} panHandlers={null} title="Аккаунт" hideNavBar={true} />

                <Scene key="likedList" component={LikedList} panHandlers={null} title="Оценили" hideNavBar={true} />

                <Scene key="singlePost" component={SinglePost} panHandlers={null} title="Пост" hideNavBar={true} />

                <Scene key="userPostsList" component={UserPostsList} panHandlers={null} hideNavBar={true} />

                <Scene key="userLikedList" component={UserLikedList} panHandlers={null} hideNavBar={true} />
                <Scene key="userModeratedList" component={UserModeratedList} panHandlers={null} hideNavBar={true} />
            </Scene>
        </Router>
    );
};

export default RouterComponent;
