export class Notification {
    type: number; // NOTIFICATION_TYPE from ../constants
    likeAuthorName: string;
    likeAuthorPhotoURL: string;
    likeAuthorUid: string;
    postShort: string;
    postId: string;
    commentShort: string;
    commentId: string;
    time: Date;
}
