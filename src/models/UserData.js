export class UserData {
    displayName: string;
    about: string;
    gender: string;
    photoURL: string;
}
