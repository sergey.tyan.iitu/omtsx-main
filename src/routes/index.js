import React from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    TouchableNativeFeedback,
    Platform
} from 'react-native';
import { Router, Scene } from 'react-native-router-flux';
import StartScreen from './StartScreen';
import LoginPage from './login';
import Registration from './registration';
import Forgot from './forgot';
import Slider from './slider';
import News from './news';
import Single from './random';
import Notifications from './notifications';
import New from './new';
import Comments from './comments';
import Categories from './categories';
import Account from './account';
import CatSelect from './catSelect';
import Settings from './include/settings';
import Subscribers from './subscribers';
import User from './user';
import Faq from './faq';
import Agreement from './agreement';
import Icon from 'react-native-vector-icons/Ionicons';

var TouchableElement = TouchableHighlight;
if (Platform.OS === 'android') TouchableElement = TouchableNativeFeedback;

let backButtonFunction = function() {
    return (
        <TouchableElement underlayColor="transparent" onPress={() => {}}>
            <Icon style={{ color: '#FFF' }} name={'md-menu'} size={26} />
        </TouchableElement>
    );
};

let rightButtonFunction = function() {
    return (
        <TouchableElement underlayColor="transparent" onPress={() => {}}>
            <Icon style={{ color: '#FFF' }} name={'md-add'} size={26} />
        </TouchableElement>
    );
};

const Index = () => {
    return (
        <Router
            navigationBarStyle={styles.navBar}
            leftButtonIconStyle={styles.icon}
            titleStyle={styles.navTitle}
        >
            <Scene key="root">
                <Scene
                    panHandlers={null}
                    key="startscreen"
                    component={StartScreen}
                    title="Верстка react-native"
                    initial
                />
                <Scene
                    panHandlers={null}
                    key="login"
                    component={LoginPage}
                    title="Страница входа"
                />

                <Scene
                    panHandlers={null}
                    key="registration"
                    component={Registration}
                    title="Регистрация"
                />

                <Scene
                    panHandlers={null}
                    key="forgot"
                    component={Forgot}
                    title="Забыли пароль?"
                />

                <Scene
                    panHandlers={null}
                    key="slider"
                    component={Slider}
                    title="Слайдер"
                />

                <Scene
                    panHandlers={null}
                    key="single"
                    component={Single}
                    title="Случайная запись"
                />

                <Scene
                    panHandlers={null}
                    key="news"
                    component={News}
                    title="OMTSX"
                    hideNavBar={true}
                />

                <Scene
                    panHandlers={null}
                    key="notifications"
                    component={Notifications}
                    title="Уведомления"
                    hideNavBar={true}
                />

                <Scene
                    panHandlers={null}
                    key="new"
                    component={New}
                    title="Новая публикация"
                    hideNavBar={true}
                />

                <Scene
                    panHandlers={null}
                    key="comments"
                    component={Comments}
                    title="Комментарии"
                />
                <Scene
                    panHandlers={null}
                    key="categories"
                    component={Categories}
                    title="Категории"
                />

                <Scene
                    panHandlers={null}
                    key="account"
                    component={Account}
                    title="Аккаунт"
                    hideNavBar={true}
                />
                <Scene
                    panHandlers={null}
                    key="user"
                    component={User}
                    title="Пользователь"
                    hideNavBar={true}
                />

                <Scene
                    panHandlers={null}
                    key="catselect"
                    component={CatSelect}
                    title="Категории"
                />

                <Scene
                    panHandlers={null}
                    key="settings"
                    component={Settings}
                    title="Настройки"
                />

                <Scene
                    key="subscribers"
                    panHandlers={null}
                    component={Subscribers}
                    title="Подписчики"
                    hideNavBar={true}
                />

                <Scene
                    panHandlers={null}
                    key="faq"
                    component={Faq}
                    title="Вопросы и ответы"
                />

                <Scene
                    panHandlers={null}
                    key="agreement"
                    component={Agreement}
                    title="Пользовательское соглашение"
                />
            </Scene>
        </Router>
    );
};

const styles = StyleSheet.create({
    navBar: {
        backgroundColor: '#663332',
        borderColor: '#fff',
        borderBottomWidth: 0
    },
    navTitle: {
        color: 'white' // changing navbar title color
    },
    icon: {
        tintColor: '#fff'
    }
});

export default Index;
