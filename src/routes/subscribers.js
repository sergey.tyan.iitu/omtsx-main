/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';

import Menu from '../components/drawMenu';
import Subscribers from './include/subscribers';

export default class omtsx extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        var component = Subscribers;
        return <Menu title={this.props.title} component={component} />;
    }
}
