/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import Menu from '../components/drawMenu';
import Settings from './include/settings';

export default class omtsx extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return <Menu title="Настройки" component={Settings} />;
    }
}
