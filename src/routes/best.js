/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import Menu from '../components/drawMenu';
import ListItems from '../routes/include/list';

export default class omtsx extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return <Menu title={this.props.title} component={ListItems} />;
    }
}
