/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import {
    Text,
    View,
    Platform,
    ListView,
    LayoutAnimation,
    TouchableOpacity,
    UIManager,
    ScrollView
} from 'react-native';
import Hr from 'react-native-hr';

import NavBar from '../components/navBarAccount';

import styles from '../style.js';

const items = [
    {
        question: 'Как изменить имя?',
        answer:
            'Перейдите в настройки приложения и нажмите редактировать. На открывшемся экране Вы сможете изменить не только имя, но и другие параметры страницы.'
    },
    {
        question: 'Как изменить пароль?',
        answer:
            'Перейдите в настройки приложения и нажмите редактировать. На открывшемся экране Вы сможете изменить не только имя, но и другие параметры страницы.'
    },
    {
        question: 'Что такое рейтинг?',
        answer:
            'Перейдите в настройки приложения и нажмите редактировать. На открывшемся экране Вы сможете изменить не только имя, но и другие параметры страницы.'
    },
    {
        question: 'Как управлять черным списком?',
        answer:
            'Перейдите в настройки приложения и нажмите редактировать. На открывшемся экране Вы сможете изменить не только имя, но и другие параметры страницы.'
    }
];

export default class omtsx extends Component {
    constructor(props) {
        super(props);
        if (Platform.OS === 'android') {
            UIManager.setLayoutAnimationEnabledExperimental &&
                UIManager.setLayoutAnimationEnabledExperimental(true);
        }
        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            }),
            activeFaq: -1
        };
    }

    selectFaq(rowID) {
        LayoutAnimation.easeInEaseOut();
        this.setState(() => ({ activeFaq: rowID }));
    }

    componentDidMount() {
        this.setState({
            dataSource: this.state.dataSource.cloneWithRows(items)
        });
    }

    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar goBack title="FAQ" />
                <ScrollView>
                    <View style={styles.containerCats}>
                        <ListView
                            scrollEnabled={false}
                            dataSource={this.state.dataSource}
                            style={styles.listS}
                            renderRow={(item, sectionID, rowID) => (
                                <TouchableOpacity
                                    underlayColor="transparent"
                                    onPress={() => this.selectFaq(rowID)}
                                >
                                    <View style={styles.itemFaq}>
                                        <View>
                                            <Text style={styles.faqName}>
                                                {item.question}
                                            </Text>
                                            <Text
                                                style={
                                                    rowID ===
                                                    this.state.activeFaq
                                                        ? styles.answer
                                                        : styles.nothing
                                                }
                                            >
                                                {item.answer}
                                            </Text>
                                        </View>
                                        <Hr lineColor="#f6f2fa" />
                                    </View>
                                </TouchableOpacity>
                            )}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }
}
