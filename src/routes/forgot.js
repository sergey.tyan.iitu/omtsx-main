/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import {
    StyleSheet,
    Text,
    View,
    StatusBar,
    TextInput,
    TouchableHighlight,
    TouchableNativeFeedback,
    Platform,
    Animated
} from 'react-native';
import { Actions } from 'react-native-router-flux';

var styles = require('../style.js');

export default class omtsx extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentWillMount() {
        this.offsetTop = new Animated.Value(0);
    }

    offsetFormTop() {
        Animated.timing(this.offsetTop, {
            toValue: -140,
            duration: 200
        }).start();
    }

    offsetFormNormal() {
        Animated.timing(this.offsetTop, {
            toValue: 0,
            duration: 200
        }).start();
    }

    logIn() {}

    componentDidMount() {
        StatusBar.setBarStyle('light-content', true);
    }

    render() {
        const offsetTopStyle = { marginTop: this.offsetTop };
        var TouchableElement = TouchableHighlight;
        if (Platform.OS === 'android') {
            TouchableElement = TouchableNativeFeedback;
        }

        return (
            <View style={styles.container}>
                <Animated.View style={[styles.center, offsetTopStyle]}>
                    <Text style={styles.forgotTitle}>Не можете войти?</Text>
                    <TextInput
                        style={styles.reginput}
                        placeholder="Имя пользователя или эл. адрес"
                        onFocus={this.offsetFormTop.bind(this)}
                        onBlur={this.offsetFormNormal.bind(this)}
                        placeholderTextColor="#c7aeb2"
                        underlineColorAndroid="transparent"
                    />

                    <TouchableElement
                        underlayColor="#704140"
                        onPress={this.logIn.bind(this)}
                        style={styles.width100}
                    >
                        <View style={styles.regbutton}>
                            <Text style={styles.loginText}>Восстановить</Text>
                        </View>
                    </TouchableElement>

                    <Text style={styles.instruction}>
                        Введите имя пользователя или эл. адрес, и мы отправим
                        Вам ссылку для восстановления доступа к аккаунту.
                    </Text>
                </Animated.View>

                <View style={styles.footer}>
                    <TouchableElement
                        underlayColor="transparent"
                        onPress={() => {
                            Actions.pop();
                        }}
                    >
                        <Text style={styles.regText}>Вернуться ко входу</Text>
                    </TouchableElement>
                </View>
            </View>
        );
    }
}
