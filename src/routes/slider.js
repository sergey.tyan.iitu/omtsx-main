/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { AppRegistry, StyleSheet, Text, View, StatusBar } from 'react-native';
import Image from 'react-native-image-progress';
import Swiper from 'react-native-swiper';

var styles = require('../style.js');

export default class omtsx extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
        StatusBar.setBarStyle('light-content', true);
    }

    render() {
        return (
            <View style={styles.container}>
                <Swiper
                    dot={
                        <View
                            style={{
                                backgroundColor: '#502725',
                                width: 12,
                                height: 12,
                                borderRadius: 13,
                                marginLeft: 7,
                                marginRight: 7
                            }}
                        />
                    }
                    activeDot={
                        <View
                            style={{
                                backgroundColor: '#fff',
                                width: 12,
                                height: 12,
                                borderRadius: 13,
                                marginLeft: 7,
                                marginRight: 7
                            }}
                        />
                    }
                >
                    <View style={styles.slide1}>
                        <View style={styles.sliderLogo}>
                            <Image
                                style={styles.sliderLogoImg}
                                source={{
                                    uri: 'https://placeimg.com/335/335/any'
                                }}
                            />
                        </View>

                        <Text style={styles.sliderTitle}>
                            Постоянный поток заказов
                        </Text>

                        <Text style={styles.sliderText}>
                            Удобный и мощный инструмент поиска{'\n'}
                            легко поможет найти нужный заказ,{'\n'}
                            ориентировочный под геолокацию курьера
                        </Text>
                    </View>

                    <View style={styles.slide2}>
                        <View style={styles.sliderLogo}>
                            <Image
                                style={styles.sliderLogoImg}
                                source={{
                                    uri: 'https://placeimg.com/335/335/any'
                                }}
                            />
                        </View>

                        <Text style={styles.sliderTitle}>
                            Постоянный поток заказов
                        </Text>

                        <Text style={styles.sliderText}>
                            Удобный и мощный инструмент поиска{'\n'}
                            легко поможет найти нужный заказ,{'\n'}
                            ориентировочный под геолокацию курьера
                        </Text>
                    </View>
                    <View style={styles.slide3}>
                        <View style={styles.sliderLogo}>
                            <Image
                                style={styles.sliderLogoImg}
                                source={{
                                    uri: 'https://placeimg.com/335/335/any'
                                }}
                            />
                        </View>

                        <Text style={styles.sliderTitle}>
                            Постоянный поток заказов
                        </Text>

                        <Text style={styles.sliderText}>
                            Удобный и мощный инструмент поиска{'\n'}
                            легко поможет найти нужный заказ,{'\n'}
                            ориентировочный под геолокацию курьера
                        </Text>
                    </View>
                </Swiper>
            </View>
        );
    }
}

AppRegistry.registerComponent('omtsx', () => omtsx);
