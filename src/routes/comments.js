/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { View } from 'react-native';
import NavBarAccount from '../components/navBarAccount';
import Comments from './include/comments';
import styles from '../style.js';

export default class omtsx extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <View style={styles.container}>
                <NavBarAccount goBack title="Комментарии" />
                <Comments postId={this.props.postId} />
            </View>
        );
    }
}
