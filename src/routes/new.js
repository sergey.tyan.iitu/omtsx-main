/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    TextInput,
    TouchableHighlight,
    TouchableNativeFeedback,
    Platform,
    Animated,
    Image,
    Keyboard
} from 'react-native';
import ImagePicker from 'react-native-image-picker';
import Picker from 'react-native-picker';
import NavBar from '../components/navBarEdit';
import * as styles from '../style.js';
import Spinner from '../components/Spinner';
import { loadCategories, writeNewPost } from '../redux/actions';
import { connect } from 'react-redux';
import _ from 'lodash';

class NewPost extends Component {
    constructor(props) {
        super(props);

        this.state = {
            image: null,
            selectedCategory: null,
            title: '',
            body: ''
        };
        // this.initPickerWithProps = this.initPickerWithProps.bind(this);
    }

    componentDidMount() {
        StatusBar.setBarStyle('light-content', true);
    }

    showImagePicker() {
        ImagePicker.showImagePicker(null, response => {
            if (response.uri) {
                let source = { uri: response.uri };

                // You can also display the image using data:
                // let source = { uri: 'data:image/jpeg;base64,' + response.data };

                this.setState({ image: source });
                if (Platform.OS === 'android') {
                    Picker.init({
                        pickerCancelBtnText: 'Отмена',
                        pickerTitleText: 'Выберите категорию',
                        pickerConfirmBtnText: 'Выбрать',
                        pickerData: this.props.catNames,
                        selectedValue: [this.props.catNames[0]],
                        onPickerConfirm: data => {
                            this.setState({
                                selectedCategory: this.props.catObjects[data[0]]
                            });
                        }
                    });
                }
            }
        });
    }

    sendPost() {
        const uid = this.props.user.uid;
        const userPic = this.props.user.photoURL;
        const userName = this.props.user.displayName;
        const title = this.state.title;
        const body = this.state.body;
        const category = this.state.selectedCategory;
        let uri = null;
        let uploadUri = null;
        if (this.state.image) {
            uri = this.state.image.uri;
            uploadUri = Platform.OS === 'ios' ? uri.replace('file://', '') : uri;
        }
        if (title && body && category) {
            this.setState({
                body: '',
                category: '',
                title: '',
                image: null
            });
            this.props.writeNewPost(uid, userPic, userName, title, body, category, uploadUri, false);
        } else {
            alert('Заполните все поля');
        }
    }

    render() {
        const TouchableElement = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableHighlight;
        return (
            <View style={styles.containerNews}>
                <NavBar title="Новая публикация" send={this.sendPost.bind(this)} />

                <TouchableElement onPress={this.showImagePicker.bind(this)} underlayColor="transparent">
                    <View style={styles.section}>
                        <View style={styles.sectionItem}>
                            <View style={styles.imgEdit2}>
                                <Image source={this.state.image} style={styles.imgEdit2Img} />
                            </View>
                            <Text style={styles.basicEditText}>Изображение (не обяз.)</Text>
                        </View>
                    </View>
                </TouchableElement>

                <View style={styles.section}>
                    <TouchableElement onPress={this.openPicker.bind(this)} underlayColor="transparent">
                        <View style={styles.sectionItem}>
                            <View style={styles.textInputFullLeft}>
                                <Text
                                    style={styles.categoryInput}
                                    textColor="#697d96"
                                    underlineColorAndroid="transparent"
                                >
                                    {this.state.selectedCategory === null
                                        ? 'Категория'
                                        : this.state.selectedCategory.name}
                                </Text>
                            </View>
                        </View>
                    </TouchableElement>

                    <View style={styles.sectionItem}>
                        <View style={styles.textInputFullLeft}>
                            <TextInput
                                style={styles.profileInputFullLeft}
                                placeholder="Заголовок"
                                textColor="#697d96"
                                placeholderTextColor="#697d96"
                                underlineColorAndroid="transparent"
                                value={this.state.title}
                                onChangeText={text => this.setState({ title: text })}
                            />
                        </View>
                    </View>

                    <View style={styles.postBodyContainer}>
                        <View style={styles.postBody}>
                            <TextInput
                                style={styles.profileInputFullLeft}
                                placeholder="Описание"
                                placeholderTextColor="#697d96"
                                textColor="#697d96"
                                multiline
                                underlineColorAndroid="transparent"
                                value={this.state.body}
                                onChangeText={text => this.setState({ body: text })}
                            />
                        </View>
                    </View>
                </View>

                <View style={styles.moder}>
                    <Text style={styles.moderText}>Ваша публикация будет на модерации от 1-3 дней</Text>
                </View>

                {this.props.uploading && (
                    <View style={styles.loadingOverlay}>
                        <Spinner size="large" />
                    </View>
                )}
            </View>
        );
    }

    openPicker() {
        Keyboard.dismiss();
        if (this.props.catNames.length > 0) {
            Picker.show();
        }
    }

    initPickerWithProps(props) {
        Picker.init({
            pickerCancelBtnText: 'Отмена',
            pickerTitleText: 'Выберите категорию',
            pickerConfirmBtnText: 'Выбрать',
            pickerData: props.catNames,
            selectedValue: [props.catNames[0]],
            onPickerConfirm: data => {
                this.setState({
                    selectedCategory: this.props.catObjects[data[0]]
                });
            }
        });
        Picker.hide();
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.catNames.length > 0) {
            this.initPickerWithProps(nextProps);
        }
    }

    componentDidMount() {
        this.offsetTop = new Animated.Value(0);
        if (this.props.catNames.length === 0) {
            this.props.loadCategories();
        } else {
            this.initPickerWithProps(this.props);
        }
    }
}

const mapStateToProps = state => {
    const catNames = _.map(state.categories.all, val => {
        return val.name;
    });

    let catObjects = {};
    if (state.categories.all instanceof Array) {
        state.categories.all.forEach((cat, index) => {
            catObjects[cat.name] = {
                name: cat.name,
                id: index
            };
        });
    }

    let result = {
        catNames,
        catObjects,
        user: state.auth.user,
        uploading: state.posts.uploading
    };
    return result;
};

export default connect(mapStateToProps, {
    loadCategories,
    writeNewPost
})(NewPost);
