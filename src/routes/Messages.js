// node modules imports
import { GiftedChat, Bubble } from 'react-native-gifted-chat';
import React, { Component } from 'react';
import { View, Platform, ActivityIndicator, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import ImagePicker from 'react-native-image-picker';
import moment from 'moment';
require('moment/locale/ru'); //for import moment local language file during the application build
moment.locale('ru'); //set moment local language to zh-cn

// local imports
import {
    stopWatchingMessages,
    fetchMessages,
    sendMessage,
    sendImage
} from '../redux/actions/ChatActions';
import { chatBubbleColor } from '../constants/index';
import { width, height } from '../constants';
import NavBar from '../components/navBarAccount';

const messageStyles = StyleSheet.create({
    containerChat: {
        flex: 1,
        backgroundColor: '#f2f2f2'
    },
    loadingStyle: {
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 20,
        width,
        height,
        backgroundColor: 'rgba(52, 52, 52, 0.8)',
        justifyContent: 'center'
    },
    rightBubble: {
        backgroundColor: chatBubbleColor
    },
    leftBubble: {
        backgroundColor: 'white'
    }
});

class Messages extends Component {
    constructor(props) {
        super(props);
        this.state = {
            count: 10
        };
        this.onSend = this.onSend.bind(this);
        this.showImagePicker = this.showImagePicker.bind(this);
        this.loadEarlier = this.loadEarlier.bind(this);
    }

    onSend(messages = []) {
        console.log();
        let message = messages[0].text;
        this.props.sendMessage(
            this.props.uid,
            this.props.userName,
            this.props.photoURL,
            this.props.recipientUid,
            this.props.recipientName,
            this.props.recipientPhotoURL,
            message
        );
    }

    showImagePicker() {
        ImagePicker.showImagePicker(
            { title: 'Отправить фотографию' },
            response => {
                if (response.uri) {
                    this.props.sendImage(
                        this.props.uid,
                        this.props.userName,
                        this.props.photoURL,
                        this.props.recipientUid,
                        this.props.recipientName,
                        this.props.recipientPhotoURL,
                        response.uri
                    );
                }
            }
        );
    }

    renderBubble(props) {
        const bubbleStyles = {
            right: messageStyles.rightBubble,
            left: messageStyles.leftBubble
        };

        return <Bubble {...props} wrapperStyle={bubbleStyles} />;
    }

    loadEarlier() {
        const fetchMessages = () =>
            this.props.fetchMessages(
                this.props.uid,
                this.props.recipientUid,
                this.state.count
            );
        this.setState({ count: this.state.count + 20 }, fetchMessages);
    }

    renderActivityIndicator() {
        if (this.props.imageUploading) {
            return (
                <View style={messageStyles.loadingStyle}>
                    <ActivityIndicator size={'large'} />
                </View>
            );
        }
    }

    render() {
        return (
            <View style={messageStyles.containerChat}>
                {Platform.OS === 'ios' && <NavBar goBack />}

                {this.renderActivityIndicator()}

                <GiftedChat
                    onLoadEarlier={this.loadEarlier}
                    loadEarlier
                    locale="ru"
                    messages={this.props.messagesFormatted || []}
                    onSend={this.onSend}
                    renderBubble={this.renderBubble}
                    user={{
                        _id: this.props.uid
                    }}
                    onPressActionButton={this.showImagePicker}
                />
            </View>
        );
    }

    componentWillMount() {
        this.props.fetchMessages(
            this.props.uid,
            this.props.recipientUid,
            this.state.count
        );
    }

    componentWillUnmount() {
        this.props.stopWatchingMessages(
            this.props.uid,
            this.props.recipientUid
        );
    }
}

const mapStateToProps = ({ chat, auth }) => {
    const { messages, loading, imageUploading } = chat;

    let messagesFormatted = _.map(messages, (msgRaw, messageId) => {
        return {
            _id: messageId,
            text: msgRaw.text,
            image: msgRaw.image,
            createdAt: new Date(msgRaw.createdAt),
            user: {
                _id: msgRaw.uid,
                name: msgRaw.name,
                avatar: msgRaw.imageUrl
            }
        };
    });

    return {
        messagesFormatted: messagesFormatted.reverse(),
        loading,
        imageUploading,
        uid: auth.user.uid,
        userName: auth.user.displayName,
        photoURL: auth.user.photoURL
    };
};

export default connect(mapStateToProps, {
    stopWatchingMessages,
    fetchMessages,
    sendMessage,
    sendImage
})(Messages);
