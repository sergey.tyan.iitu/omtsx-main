/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { StyleSheet, View, Platform, ListView, TouchableOpacity, Text } from 'react-native';
import axios from 'axios';
import _ from 'lodash';
import { Actions } from 'react-native-router-flux';

import { width } from '../../constants';
import Spinner from '../../components/Spinner';
import NavBar from '../../components/navBarAccount';
import * as styles from '../../style.js';

const style = StyleSheet.create({
    button: {
        flex: 1,
        height: 40,
        backgroundColor: 'white',
        width: width - 14
    },
    list: { alignItems: 'center' }
});

export default class UserLikedList extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.state = {
            loading: true,
            dataSource: ds.cloneWithRows({})
        };
        console.log(props);
        this.fetchUserLikedPosts = this.fetchUserLikedPosts.bind(this);
    }

    fetchUserLikedPosts() {
        axios
            .get(`https://blinding-heat-7224.firebaseio.com/user-info/${this.props.currentUser.uid}/likes.json`)
            .then(res => {
                console.log(res.data);
                let posts = _.map(res.data, (text, postId) => {
                    return { text, postId };
                });

                console.log(posts);

                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(posts),
                    loading: false
                });
            })
            .catch(err => {
                console.log(err);
                this.setState({ loading: false });
            });
    }

    renderItem(item) {
        const onPress = () => {
            Actions.singlePost({ postId: item.postId });
        };
        return (
            <TouchableOpacity underlayColor="rgba(0,0,0,0.05)" onPress={onPress} style={style.button}>
                <View style={styles.itemProfile}>
                    <View style={styles.itemProfileCenter}>
                        <Text style={styles.itemProfileCenterText}>{item.text}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    renderLoading() {
        if (this.state.loading && Platform.OS === 'ios') {
            return <Spinner size="large" />;
        }
    }

    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar goBack title="Понравилось" />

                <View>
                    <ListView
                        contentContainerStyle={style.list}
                        dataSource={this.state.dataSource}
                        renderRow={this.renderItem.bind(this)}
                        style={styles.listS}
                        scrollbarStyle="outsideOverlay"
                        enableEmptySections
                    />

                    {this.renderLoading()}
                </View>
            </View>
        );
    }

    componentDidMount() {
        this.fetchUserLikedPosts();
    }
}
