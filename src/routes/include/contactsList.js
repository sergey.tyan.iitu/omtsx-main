/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import {
    Text,
    View,
    TouchableHighlight,
    TouchableNativeFeedback,
    Platform,
    ListView,
    Image,
} from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Actions } from 'react-native-router-flux';
import styles from '../../style.js';
import Spinner from '../../components/Spinner';
import NavBar from '../../components/navBar';
import {
    fetchContacts,
    stopWatchingContacts
} from '../../redux/actions/ChatActions';
import moment from 'moment';

require('moment/locale/ru'); //for import moment local language file during the application build
moment.locale('ru');//set moment local language to zh-cn
const TouchableElement = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableHighlight;

class ContactsList extends Component {

    constructor(props) {
        super(props);
    }

    renderItem = (item) => {
        const time = moment(item.lastUpdateDate, 'YYYY.MM.DD HH:mm').fromNow(false);
        return (
            <TouchableElement
                onPress={() => Actions.messages({
                    recipientUid: item.uid,
                    recipientName: item.name,
                    recipientPhotoURL: item.imageUrl
                })}
            >
                <View style={styles.itemN}>

                    <View style={styles.nAvatar}>
                        <View style={styles.imgProfileN}>
                            <Image source={{uri: item.imageUrl}} style={styles.itemImageN}/>
                        </View>
                        {item.unreadCount > 0 && (
                            <View
                                style={{
                                    width: 25,
                                    height: 25,
                                    borderRadius: 25,
                                    backgroundColor: '#663333',
                                    borderWidth: 1,
                                    borderColor: '#fff',
                                    justifyContent: 'center',
                                    position: 'absolute',
                                    right: 14,
                                    bottom: 0
                                }}>
                                <Text
                                    style={styles.transparent}
                                    size={13}>
                                    {item.unreadCount}
                                </Text>
                            </View>)
                        }
                    </View>

                    <View style={styles.nDesc}>
                        <View style={styles.headN}>
                            <Text style={styles.nameItemN}>{item.name}</Text>
                        </View>

                        {item.lastMessage &&
                        (<Text style={styles.txtCommentItemN}>{item.lastMessage}</Text>)
                        }

                        <View style={styles.footN}>
                            <Text style={styles.timeItemN}>{time} </Text>
                        </View>
                    </View>
                </View>
            </TouchableElement>
        )
    };

    renderLoading() {
        if (this.props.loading && (Platform.OS === 'ios')) {
            return <Spinner size="large"/>
        }
    };

    render() {


        return (
            <View style={styles.containerNews}>
                <NavBar title={this.props.title}
                        openDrawer={this.props.openDrawer.bind(this)}
                />

                <View>
                    <ListView
                        dataSource={this.dataSource}
                        renderRow={this.renderItem.bind(this)}
                        style={styles.listS}
                        scrollbarStyle="outsideOverlay"
                        enableEmptySections
                        contentContainerStyle={{alignItems: 'center'}}
                    />

                    {this.renderLoading()}

                </View>

            </View>
        );
    }

    componentWillReceiveProps(nextProps) {
        this.createDataSource(nextProps);
    }

    createDataSource({contacts}) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(contacts);
    }

    componentWillMount() {
        this.props.fetchContacts(this.props.uid);
        this.createDataSource(this.props);
    }

    componentWillUnmount() {
        this.props.stopWatchingContacts(this.props.uid);
    }
}

const mapStateToProps = (state) => {
    let contacts = _.map(state.chat.contacts, (val) => {
        return {...val};
    });
    contacts = _.orderBy(contacts, ['lastUpdateDate']).reverse();

    let result = {
        uid: state.auth.user.uid,
        contacts,
        loading: state.chat.loading
    };
    return result;
};

export default connect(mapStateToProps, {
    fetchContacts,
    stopWatchingContacts
})(ContactsList);

