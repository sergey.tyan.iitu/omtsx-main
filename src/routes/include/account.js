/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Text,
    View,
    Platform,
    Dimensions,
    TouchableOpacity,
    Modal,
    TextInput,
    Animated,
    Switch,
    StyleSheet
} from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import Image from 'react-native-image-progress';
import ParallaxView from 'react-native-parallax-view';
import ImagePicker from 'react-native-image-picker';
import { Actions } from 'react-native-router-flux';

import {
    getUserInfo,
    editUserPhoto,
    stopWatchingUserInfo,
    editUserBackgroundPhoto,
    editUserData
} from '../../redux/actions/UserInfoActions';
import styles from '../../style.js';
import NavBarAccount from '../../components/navBarAccount';
import NavBar from '../../components/navBarEdit';
import { UserData } from '../../models/UserData';

const width = Dimensions.get('window').width;

const accountStyle = StyleSheet.create({
    switch: {
        alignSelf: 'flex-end',
        marginRight: 20,
        marginBottom: 8
    },
    commentBtn: {
        flex: 1,
        height: 40
    }
});

class Account extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            genderMale: true
        };
        this.updateUserData = this.updateUserData.bind(this);
        this.openUserListPosts = this.openUserListPosts.bind(this);
        this.openUserLikedPosts = this.openUserLikedPosts.bind(this);
        this.openUserModeratedPosts = this.openUserModeratedPosts.bind(this);
    }

    componentDidMount() {}

    isMyAccount() {
        return this.props.currentUser.uid === this.props.loadUserId || this.props.loadUserId === undefined;
    }

    changePhoto() {
        if (this.isMyAccount()) {
            ImagePicker.showImagePicker({ title: 'Сменить фото профиля?' }, response => {
                if (response.uri) {
                    this.props.editUserPhoto(this.props.currentUser, response.uri);
                }
            });
        }
    }

    renderHeader() {
        return (
            <View style={styles.listProfileHeader}>
                <View style={styles.userMainImage}>
                    <Image
                        style={Platform.OS === 'android' ? styles.userBg : styles.nothing}
                        source={
                            this.props.backgroundPhotoURL
                                ? { uri: this.props.backgroundPhotoURL }
                                : { uri: 'https://placeimg.com/' + width + '/215/any' }
                        }
                    />
                    <View style={Platform.OS === 'android' ? styles.accountInfoOne : styles.accountInfo}>
                        <View style={styles.accountAvatar}>
                            <Image
                                style={styles.accountAvatarImage}
                                source={
                                    this.props.photoURL
                                        ? { uri: this.props.photoURL }
                                        : { uri: 'https://placeimg.com/100/100/any' }
                                }
                            />
                        </View>

                        <View style={styles.accountStatus}>
                            <Text style={styles.accountStatusText}>{this.props.about}</Text>
                        </View>

                        {this.isMyAccount() && (
                            <TouchableOpacity
                                onPress={() => {
                                    this.setState({ modalVisible: true });
                                }}
                            >
                                <View style={styles.editButton}>
                                    <Icon color="#7889a0" name="md-create" style={styles.editIcon} size={10} />
                                    <Text style={styles.editButtonText}>Редактировать</Text>
                                </View>
                            </TouchableOpacity>
                        )}
                    </View>
                </View>
            </View>
        );
    }

    changeBackgroundPhoto() {
        if (this.isMyAccount()) {
            ImagePicker.showImagePicker({ title: 'Сменить фото обложки?' }, response => {
                if (response.uri) {
                    this.props.editUserBackgroundPhoto(this.props.currentUser, response.uri);
                }
            });
        }
    }

    updateUserData() {
        let data = new UserData();
        data.about = this.state.newAbout || this.props.about;
        data.gender = this.state.genderMale ? 'M' : 'F';
        data.displayName = this.state.newUserName || this.props.displayName;

        if (this.isMyAccount()) {
            this.props.editUserData(this.props.currentUser, data);
        }
        this.setState({ modalVisible: false });
    }

    dismissModal() {
        this.setState({ modalVisible: false });
    }

    offsetFormTop() {
        Animated.timing(this.offsetTop, {
            toValue: -140,
            duration: 200
        }).start();
    }

    offsetFormNormal() {
        Animated.timing(this.offsetTop, {
            toValue: 0,
            duration: 200
        }).start();
    }

    renderModal() {
        const offsetTopStyle = { marginTop: this.offsetTop };
        return (
            <Animated.View style={[styles.containerNews, offsetTopStyle]}>
                <NavBar
                    dismiss={this.dismissModal.bind(this)}
                    title="Редактирование"
                    send={this.updateUserData}
                    sendText={'Сохранить'}
                />
                <View style={styles.section}>
                    <TouchableOpacity onPress={this.changePhoto.bind(this)}>
                        <View style={styles.sectionItem}>
                            <View style={styles.imgEdit1}>
                                <Image source={{ uri: this.props.photoURL }} style={styles.imgEdit1Img} />
                            </View>
                            <Text style={styles.basicEditText}>Изображение профиля</Text>
                        </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={this.changeBackgroundPhoto.bind(this)}>
                        <View style={styles.sectionItem}>
                            <View style={styles.imgEdit2}>
                                <Image source={{ uri: this.props.backgroundPhotoURL }} style={styles.imgEdit2Img} />
                            </View>
                            <Text style={styles.basicEditText}>Изображение на обложке</Text>
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={styles.section}>
                    <View style={styles.sectionItem}>
                        <View style={styles.label}>
                            <Text style={styles.textLabel}>Username</Text>
                        </View>
                        <View style={styles.textInput}>
                            <TextInput
                                style={styles.profileInput}
                                underlineColorAndroid="transparent"
                                onChangeText={text => this.setState({ newUserName: text })}
                                defaultValue={this.props.displayName}
                                onFocus={this.offsetFormTop.bind(this)}
                                onBlur={this.offsetFormNormal.bind(this)}
                                returnKeyType={'done'}
                            />
                        </View>
                    </View>

                    <View style={styles.sectionItem}>
                        <View style={styles.label}>
                            <Text style={styles.textLabel}>
                                {this.state.genderMale ? 'Пол: мужской' : 'Пол: женский'}
                            </Text>
                        </View>
                        <View style={accountStyle.switch}>
                            <Switch
                                tintColor={'pink'}
                                onTintColor={'#42aaf4'}
                                thumbTintColor={'white'}
                                value={this.state.genderMale}
                                onValueChange={() => this.setState({ genderMale: !this.state.genderMale })}
                            />
                        </View>
                    </View>

                    <View style={styles.sectionItem}>
                        <View style={styles.label}>
                            <Text style={styles.textLabel}>Почта</Text>
                        </View>
                        <View style={styles.textInput}>
                            <TextInput
                                style={styles.profileInput}
                                defaultValue={this.props.currentUser.email}
                                onChangeText={text => this.setState({ newEmail: text })}
                                placeholderTextColor="#697d96"
                                underlineColorAndroid="transparent"
                                onFocus={this.offsetFormTop.bind(this)}
                                onBlur={this.offsetFormNormal.bind(this)}
                                returnKeyType={'done'}
                            />
                        </View>
                    </View>

                    <View style={styles.sectionItem}>
                        <View style={styles.textInputFull}>
                            <TextInput
                                style={styles.profileInputFull}
                                placeholderTextColor="#697d96"
                                underlineColorAndroid="transparent"
                                onChangeText={text => this.setState({ newAbout: text })}
                                defaultValue={this.props.about}
                                onFocus={this.offsetFormTop.bind(this)}
                                onBlur={this.offsetFormNormal.bind(this)}
                                returnKeyType={'done'}
                            />
                        </View>
                    </View>

                    {/*TODO*/}
                    {/*<View style={styles.sectionItem}>*/}
                    {/*<View style={styles.label}>*/}
                    {/*<Text style={styles.textLabel}>Город</Text>*/}
                    {/*</View>*/}
                    {/*<View style={styles.textInput}>*/}
                    {/*<TextInput*/}
                    {/*style={styles.profileInput}*/}
                    {/*defaultValue="г. Алматы"*/}
                    {/*placeholderTextColor="#697d96"*/}
                    {/*underlineColorAndroid='transparent'*/}
                    {/*/>*/}
                    {/*</View>*/}
                    {/*</View>*/}
                </View>
            </Animated.View>
        );
    }

    openChat() {
        if (this.props.currentUser.uid !== this.props.loadUserId) {
            Actions.messages({
                recipientUid: this.props.userId,
                recipientName: this.props.displayName,
                recipientPhotoURL: this.props.photoURL
            });
        }
    }

    openUserListPosts() {
        Actions.userPostsList({ currentUser: this.props.currentUser });
    }

    openUserLikedPosts() {
        Actions.userLikedList({ currentUser: this.props.currentUser });
    }

    openUserModeratedPosts() {
        Actions.userModeratedList({ currentUser: this.props.currentUser });
    }

    renderMenuItem(iconName, onPress, text) {
        return (
            <TouchableOpacity underlayColor="rgba(0,0,0,0.05)" onPress={onPress} style={accountStyle.commentBtn}>
                <View style={styles.itemProfile}>
                    <View style={styles.itemProfileLeft}>
                        <Icon style={styles.itemProfileLeftIcon} color="#8c98ab" name={iconName} size={24} />
                    </View>
                    <View style={styles.itemProfileCenter}>
                        <Text style={styles.itemProfileCenterText}>{text}</Text>
                    </View>
                    <View style={styles.itemProfileRight}>
                        <View style={styles.itemProfileRightWrap}>
                            <Text style={styles.itemProfileRightText}>→</Text>
                        </View>
                    </View>
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.containerNews}>
                <Modal
                    animationType={'fade'}
                    transparent={true}
                    visible={this.state.modalVisible}
                    onRequestClose={() => {
                        this.setState({ modalVisible: false });
                    }}
                >
                    {this.renderModal()}
                </Modal>

                <NavBarAccount
                    goBack={this.props.openDrawer === undefined}
                    rightIcon={this.isMyAccount() ? null : 'md-text'}
                    title={this.props.displayName}
                    rightIconPress={this.openChat.bind(this)}
                    openDrawer={this.props.openDrawer ? this.props.openDrawer.bind(this) : null}
                />

                <ParallaxView
                    backgroundSource={{ uri: 'https://placeimg.com/' + width + '/215/any' }}
                    windowHeight={215}
                    scrollableViewStyle={styles.viewUser}
                    header={this.renderHeader()}
                >
                    <View style={styles.itemsProfile}>
                        {this.renderMenuItem('md-clipboard', this.openUserListPosts, 'Мои записи')}
                        {this.renderMenuItem('md-time', this.openUserModeratedPosts, 'На модерации')}
                        {this.renderMenuItem('md-heart', this.openUserLikedPosts, 'Понравилось')}
                    </View>
                </ParallaxView>
            </View>
        );
    }

    componentWillMount() {
        this.offsetTop = new Animated.Value(0);
        if (this.props.loadUserId) {
            this.props.getUserInfo(this.props.loadUserId);
        } else {
            this.props.getUserInfo(this.props.currentUser.uid);
        }
        // this.props.getUserInfo('GJN1ztV7aqTe3BW27qnYbWIpLrq2');
    }

    componentWillUnmount() {
        if (this.props.loadUserId) {
            this.props.stopWatchingUserInfo(this.props.loadUserId);
        } else {
            this.props.stopWatchingUserInfo(this.props.currentUser.uid);
        }
    }
}

const mapStateToProps = ({ userInfo, auth }) => {
    const result = {
        ...userInfo,
        currentUser: auth.user
    };
    return result;
};

export default connect(mapStateToProps, {
    getUserInfo,
    editUserPhoto,
    editUserBackgroundPhoto,
    editUserData,
    stopWatchingUserInfo
})(Account);
