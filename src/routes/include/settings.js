/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import NavBar from '../../components/navBar';
import { Actions } from 'react-native-router-flux';
import * as styles from '../../style.js';
import C from '../../redux/actions/types';
import * as firebase from 'firebase';

const settingStyles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        height: 45,

        borderBottomColor: '#ecebec',
        borderRadius: 5,
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    catIcon: {
        position: 'absolute',
        right: 10,
        top: 13
    }
});

export default class Settings extends Component {
    constructor(props) {
        super(props);

        this.state = {};
        this.signOut = this.signOut.bind(this);
    }

    signOut() {
        const uid = firebase.auth().currentUser.uid;

        firebase
            .database()
            .ref(C.F_USER_NOTIFICATIONS + uid)
            .off();

        firebase
            .auth()
            .signOut()
            .then(
                () => Actions.auth(),
                error => {
                    alert(error);
                }
            );
    }

    // TODO FIX styles.sectionItem
    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar openDrawer={this.props.openDrawer.bind(this)} />

                <View style={styles.section}>
                    <TouchableOpacity underlayColor="rgba(0,0,0,0.1)" onPress={() => Actions.account()}>
                        <View style={settingStyles.item}>
                            <Text style={styles.catName}>Учетная запись</Text>
                            <Icon style={settingStyles.catIcon} color="#8c98ab" name="ios-arrow-forward" size={18} />
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={styles.sectionLabelBottom}>
                    <Text style={styles.sectionLabelText}>
                        При привязанном аккаунте Инстаграм у Вас будет возможность публиковать.
                    </Text>
                </View>
                <View style={styles.section}>
                    <TouchableOpacity underlayColor="rgba(0,0,0,0.1)" onPress={() => Actions.catSelect()}>
                        <View style={settingStyles.item}>
                            <Text style={styles.catName}>Настройки категории</Text>
                            <Icon style={settingStyles.catIcon} color="#8c98ab" name="ios-arrow-forward" size={18} />
                        </View>
                    </TouchableOpacity>
                </View>

                <View style={styles.sectionLabelBottom}>
                    <Text style={styles.sectionLabelText}>Выберите категории для отображения</Text>
                </View>
                <View style={styles.section}>
                    <TouchableOpacity underlayColor="rgba(0,0,0,0.1)" onPress={() => Actions.faq()}>
                        <View style={settingStyles.item}>
                            <Text style={styles.catName}>Часто задаваемые вопросы</Text>
                            <Icon style={settingStyles.catIcon} color="#8c98ab" name="ios-arrow-forward" size={18} />
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity underlayColor="rgba(0,0,0,0.1)" onPress={() => Actions.agreement()}>
                        <View style={settingStyles.item}>
                            <Text style={styles.catName}>Пользовательское соглашение</Text>
                            <Icon style={settingStyles.catIcon} color="#8c98ab" name="ios-arrow-forward" size={18} />
                        </View>
                    </TouchableOpacity>
                </View>
                <View style={styles.sectionLabelBottom}>
                    <Text style={styles.sectionLabelText}>
                        Использование приложения подразумевает согласие с пользовательским соглашением.
                    </Text>
                </View>
                <View style={styles.section}>
                    <TouchableOpacity underlayColor="rgba(0,0,0,0.1)" onPress={this.signOut}>
                        <View style={styles.sectionItemCenter}>
                            <Text style={styles.exit}>Выход</Text>
                        </View>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
