import React, { Component } from 'react';
import { Text, View, TouchableOpacity, TouchableHighlight, TouchableNativeFeedback, Platform } from 'react-native';
import Image from 'react-native-image-progress';
import Hr from 'react-native-hr';
import Icon from 'react-native-vector-icons/Ionicons';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';

require('moment/locale/ru'); //for import moment local language file during the application build
moment.locale('ru'); //set moment local language to zh-cn
import { toggleLike } from '../../redux/actions/PostsActions';
import * as styles from '../../style.js';
import { TIME_FORMAT } from '../../constants/index';

const TouchableElement = Platform.OS === 'android' ? TouchableNativeFeedback : TouchableHighlight;
export default class PostItem extends Component {
    constructor(props) {
        super(props);
    }

    openProfile(userId) {
        Actions.accountPage({ loadUserId: userId });
    }

    onLikePressed(postItem) {
        const postShort = postItem.body.length > 140 ? `${postItem.body.substring(0, 140)}...` : postItem.body;
        toggleLike(
            postItem.postId,
            postShort,
            postItem.category ? postItem.category.id : null,
            postItem.uid,
            this.props.currentUser.uid,
            this.props.currentUser.displayName || 'empty',
            this.props.currentUser.photoURL
        );
        if (this.props.afterLike) {
            this.props.afterLike();
        }
    }

    render() {
        // console.log('post item render');
        const item = this.props.item;
        if (item) {
            let likedByMe = false;
            if (item.likes !== undefined) {
                likedByMe = item.likes[this.props.currentUser.uid] !== undefined;
            }

            const postingTime = moment(item.postingTime, TIME_FORMAT).fromNow(false);

            return (
                <View style={styles.item}>
                    <View style={styles.headItem}>
                        <View style={styles.imgItem}>
                            <Image
                                source={{ uri: item.userPic ? item.userPic : 'https://placeimg.com/75/75/any' }}
                                style={styles.itemImage}
                            />
                        </View>
                        <TouchableElement onPress={() => this.openProfile(item.uid)}>
                            <View style={styles.titleItemWr}>
                                <Text style={styles.titleItem}>{item.userName || 'empty username'}</Text>
                                <Text style={styles.catItem}>{postingTime !== 'Invalid date' ? postingTime : ''}</Text>
                            </View>
                        </TouchableElement>
                        {/*<Modal/>*/}
                        <TouchableOpacity
                            style={styles.contextModal}
                            onPress={() => Actions.likedList({ postId: item.postId })}
                        >
                            <Icon name={'ios-more'} color="#babfc3" style={styles.contextIcon} size={32} />
                        </TouchableOpacity>
                    </View>

                    {item.image !== undefined && <Image source={{ uri: item.image }} style={styles.itemMainImage} />}

                    <View>
                        <Text style={styles.titleContent}>{item.title}</Text>
                        <Text style={styles.textContent}>{item.body}</Text>
                        <Hr lineColor="#f6f2fa" />

                        <View style={styles.soc}>
                            <TouchableElement underlayColor="transparent" onPress={() => this.onLikePressed(item)}>
                                <View style={{ flexDirection: 'row' }}>
                                    <Icon
                                        style={styles.likesIcon}
                                        color={likedByMe ? '#653430' : '#c1c4c9'}
                                        name={likedByMe ? 'ios-heart' : 'ios-heart-outline'}
                                        size={25}
                                    />
                                    <Text style={styles.likesCount}>{item.likeCount || 0}</Text>
                                </View>
                            </TouchableElement>
                            <TouchableElement
                                underlayColor="transparent"
                                onPress={() => Actions.comments({ postId: item.postId })}
                            >
                                <View>
                                    <Icon style={styles.commentsIcon} color="#c1c4c9" name={'md-chatboxes'} size={25} />
                                    <Text style={styles.commentsCount}>{item.comments}</Text>
                                </View>
                            </TouchableElement>

                            <Text style={item.category !== '' ? styles.category : styles.nothing}>
                                {item.category ? item.category.name : ''}
                            </Text>
                        </View>
                    </View>
                </View>
            );
        } else {
            return <View />;
        }
    }
}
