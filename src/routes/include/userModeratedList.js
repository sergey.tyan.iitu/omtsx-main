/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { View, Platform, ListView } from 'react-native';
import _ from 'lodash';
import * as firebase from 'firebase';

import PostItem from './PostItem';
import Spinner from '../../components/Spinner';
import NavBar from '../../components/navBarAccount';

import * as styles from '../../style.js';
import { F_POSTS_FOR_MODERATION } from '../../redux/actions/types';

export default class UserModeratedList extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.state = {
            loading: true,
            dataSource: ds.cloneWithRows({})
        };
        console.log(props);

        this.fetchUserModeratedPosts = this.fetchUserModeratedPosts.bind(this);
    }

    fetchUserModeratedPosts() {
        const userModeratedPosts = firebase
            .database()
            .ref(F_POSTS_FOR_MODERATION)
            .orderByChild('uid')
            .equalTo(this.props.currentUser.uid);

        userModeratedPosts.once(
            'value',
            response => {
                console.log(response.val());
                let posts = _.map(response.val(), (val, postId) => {
                    return { ...val, postId };
                }).reverse();

                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(posts),
                    loading: false
                });
            },
            error => {
                console.log(error);
                this.setState({ loading: false });
            }
        );
    }

    renderItem(item) {
        return <PostItem item={item} currentUser={this.props.currentUser} />;
    }

    renderLoading() {
        if (this.state.loading && Platform.OS === 'ios') {
            return <Spinner size="large" />;
        }
    }

    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar goBack title="Мои посты" />

                <View>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.renderItem.bind(this)}
                        style={styles.listS}
                        scrollbarStyle="outsideOverlay"
                        enableEmptySections
                    />

                    {this.renderLoading()}
                </View>
            </View>
        );
    }

    componentDidMount() {
        this.fetchUserModeratedPosts();
    }
}
