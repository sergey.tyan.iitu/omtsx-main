/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    View,
    ListView
} from 'react-native';
import { connect } from 'react-redux';

import PostItem from './PostItem';
import * as styles from '../../style.js';
import NavBar from '../../components/navBarAccount';
import { fetchSinglePublishedPost } from '../../redux/actions';

class SinglePost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            dataSource: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            }),
        };
    }

    renderItem(item) {
        return (<PostItem item={item} currentUser={this.props.currentUser}/>)
    }

    render() {
        return (

            <View style={{flex:1, backgroundColor: '#f2f2f2'}}>

                <NavBar goBack title="Post"/>
                {/*TODO figure why single PostItem styles don't work*/}
                {/*<View style={styles.list}>*/}
                    {/*<PostItem item={this.props.post} currentUser={this.props.currentUser}/>*/}
                {/*</View>*/}

                <ListView
                    scrollbarStyle='outsideOverlay'
                    dataSource={this.state.dataSource}
                    renderRow={this.renderItem.bind(this)}
                    style={styles.listS}
                />

            </View>
        );
    }

    createDataSource(post) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        let item = [post];

        this.setState({dataSource: ds.cloneWithRows(item)});
    }

    componentWillReceiveProps(nextProps) {
        this.createDataSource(nextProps.post);
    }

    componentWillMount() {

        this.props.fetchSinglePublishedPost(this.props.postId);
        this.createDataSource({});
    }
}

const mapStateToProps = (state) => {
    const result = {
        post: state.posts.singlePost,
        currentUser: state.auth.user
    };
    console.log(result);
    return result;
};

export default connect(mapStateToProps, {
    fetchSinglePublishedPost
})(SinglePost);