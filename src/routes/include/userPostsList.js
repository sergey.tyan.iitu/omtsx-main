/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { View, Platform, ListView } from 'react-native';
import axios from 'axios';
import _ from 'lodash';
import PostItem from './PostItem';
import Spinner from '../../components/Spinner';
import NavBar from '../../components/navBarAccount';

import * as styles from '../../style.js';

export default class UserPostsList extends Component {
    constructor(props) {
        super(props);
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.state = {
            loading: true,
            dataSource: ds.cloneWithRows({})
        };
        console.log(props);
        this.fetchUserPosts = this.fetchUserPosts.bind(this);
    }

    fetchUserPosts() {
        axios
            .get(`https://blinding-heat-7224.firebaseio.com/user-posts/${this.props.currentUser.uid}.json`)
            .then(res => {
                let posts = _.map(res.data, (val, postId) => {
                    return { ...val, postId };
                });

                this.setState({
                    dataSource: this.state.dataSource.cloneWithRows(posts),
                    loading: false
                });
            })
            .catch(err => {
                console.log(err);
                this.setState({ loading: false });
            });
    }

    renderItem(item) {
        return <PostItem item={item} currentUser={this.props.currentUser} />;
    }

    renderLoading() {
        if (this.state.loading && Platform.OS === 'ios') {
            return <Spinner size="large" />;
        }
    }

    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar goBack title="Мои посты" />

                <View>
                    <ListView
                        dataSource={this.state.dataSource}
                        renderRow={this.renderItem.bind(this)}
                        style={styles.listS}
                        scrollbarStyle="outsideOverlay"
                        enableEmptySections
                    />

                    {this.renderLoading()}
                </View>
            </View>
        );
    }

    componentDidMount() {
        this.fetchUserPosts();
    }
}
