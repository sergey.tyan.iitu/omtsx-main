/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ListView } from 'react-native';
import Image from 'react-native-image-progress';
import Icon from 'react-native-vector-icons/Ionicons';
import NavBar from '../../components/navBar';
import { NOTIFICATION_DESCRIPTION, NOTIFICATION_TYPE, TIME_FORMAT } from '../../constants';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import moment from 'moment';

require('moment/locale/ru'); //for import moment local language file during the application build
moment.locale('ru'); //set moment local language to zh-cn
import _ from 'lodash';
import { getNotifications } from '../../redux/actions/UserInfoActions';

const styles = require('../../style.js');

const items = [
    // {
    //     type: NOTIFICATION_TYPE.post_like,
    //     name: 'Almas_best',
    //     action: 'оценил вашу',
    //     target: 'фотографию',
    //     photo: {uri: 'https://placeimg.com/75/75/any'},
    //     time: '3 часа назад',
    //     smallIconName: 'ios-heart',
    //     smallIconColor: '#663333',
    //     avatarImage: {uri: 'https://placeimg.com/75/75/any'},
    //     textComment: '',
    //     targetComment: ''
    // },
    {
        type: NOTIFICATION_TYPE.post_like,
        name: 'GuldanaaaF012',
        action: 'оценил(-а) вашу запись',
        target: 'неожиданная встреча с девушкой из буду..a sdas askdlf mlaskdm fkasmdf; klma',
        time: 'сегодня в 14:11',
        smallIconName: 'ios-heart',
        avatarImage: { uri: 'https://placeimg.com/75/75/any' }
    },
    {
        type: NOTIFICATION_TYPE.post_comment,
        name: 'Windowmaker',
        action: 'написал(-а) комментарий',
        target: 'Я как-то тоже пробовал как и ты) но потом долго жалел что сделал так)',
        time: '23 минуты назад на Вашу запись',
        smallIconName: 'md-text',
        avatarImage: { uri: 'https://placeimg.com/75/75/any' },
        targetComment: 'Как я побывал на природе'
    },
    {
        type: NOTIFICATION_TYPE.mention,
        name: 'GoldenBreeze',
        action: 'отметил(-а) вас на комментарии',
        target: 'Очень странно что вышло так))',
        time: 'сегодня в 14:11',
        smallIconName: 'md-person-add',
        avatarImage: { uri: 'https://placeimg.com/75/75/any' }
    },
    {
        type: NOTIFICATION_TYPE.story_accepted,
        target: 'Очень странно что вышло так))',
        time: 'Сегодня в 14:11',
        avatarImage: { uri: 'https://image.ibb.co/fWjDJv/ok.png' },
        action: 'Ваша запись успешно опубликована'
    },
    {
        type: NOTIFICATION_TYPE.story_declined,
        target: 'Очень странно что вышло так))',
        time: 'Сегодня в 14:11',
        avatarImage: { uri: 'https://image.ibb.co/fipDJv/cancel.png' },
        action: 'Ваша запись отклонена'
    }
];

const notificationIconStyle = {
    width: 25,
    height: 25,
    borderRadius: 25,
    borderWidth: 1,
    borderColor: '#fff',
    justifyContent: 'center',
    position: 'absolute',
    right: 14,
    bottom: 0,
    backgroundColor: '#663333'
};

class NotificationList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            notifCount: 8
        };
    }

    renderNotificationNameHeader(item) {
        return (
            <TouchableOpacity onPress={() => Actions.accountPage({ userId: item.uid })}>
                <Text style={styles.headN}>
                    <Text style={styles.nameItemN}>{item.name} </Text>
                    <Text>{item.action}</Text>
                </Text>
            </TouchableOpacity>
        );
    }

    renderPostLink(item) {
        console.log(item);
        return (
            <TouchableOpacity onPress={() => Actions.singlePost({ postId: item.postId })}>
                <Text style={styles.targetItemN}>{item.target} </Text>
            </TouchableOpacity>
        );
    }

    renderLikeNotification(item) {
        return (
            <View style={styles.itemN}>
                <View style={styles.nAvatar}>
                    <View style={styles.imgProfileN}>
                        <Image source={item.avatarImage} style={styles.itemImageN} />
                    </View>
                    <View style={notificationIconStyle}>
                        <Icon style={styles.transparent} color="#fff" name={item.smallIconName} size={13} />
                    </View>
                </View>

                <View style={styles.nDesc}>
                    {this.renderNotificationNameHeader(item)}
                    {this.renderPostLink(item)}
                    <Text style={styles.footN}>
                        <Text style={styles.timeItemN}>{item.time} </Text>
                        <Text style={styles.targetComment}>{item.targetComment}</Text>
                    </Text>
                </View>
            </View>
        );
    }

    renderCommentNotification(item) {
        return (
            <View style={styles.itemN}>
                <View style={styles.nAvatar}>
                    <View style={styles.imgProfileN}>
                        <Image source={item.avatarImage} style={styles.itemImageN} />
                    </View>
                    <View style={notificationIconStyle}>
                        <Icon style={styles.transparent} color="#fff" name={item.smallIconName} size={13} />
                    </View>
                </View>

                <View style={styles.nDesc}>
                    {this.renderNotificationNameHeader(item)}
                    <Text style={styles.targetItemN}>{item.targetComment}</Text>
                    <Text style={styles.timeItemN}>{`${item.time} на вашу запись`}</Text>
                    {this.renderPostLink(item)}
                </View>
            </View>
        );
    }

    renderMentionNotification(item) {
        return (
            <View style={styles.itemN}>
                <View style={styles.nAvatar}>
                    <View style={styles.imgProfileN}>
                        <Image source={item.avatarImage} style={styles.itemImageN} />
                    </View>
                    <View style={notificationIconStyle}>
                        <Icon style={styles.transparent} color="#fff" name={item.smallIconName} size={13} />
                    </View>
                </View>
                <View style={styles.nDesc}>
                    {this.renderNotificationNameHeader(item)}
                    {this.renderPostLink(item)}
                    <Text style={styles.footN}>
                        <Text style={styles.timeItemN}>{item.time} </Text>
                        <Text style={styles.targetComment}>{item.targetComment}</Text>
                    </Text>
                </View>
            </View>
        );
    }

    renderStoryNotification(item) {
        return (
            <View style={styles.itemN}>
                <View style={styles.nAvatar}>
                    <View style={styles.imgProfileN}>
                        <Image source={item.avatarImage} style={styles.itemImageN} />
                    </View>
                </View>
                <View style={styles.nDesc}>
                    {this.renderPostLink(item)}
                    <Text style={styles.txtCommentItemN}>{item.action} </Text>
                    <Text style={styles.timeItemN}>{item.time} </Text>
                </View>
            </View>
        );
    }

    renderItem(item) {
        switch (item.type) {
            case NOTIFICATION_TYPE.post_like:
                return this.renderLikeNotification(item);
            case NOTIFICATION_TYPE.post_comment:
                return this.renderCommentNotification(item);
            case NOTIFICATION_TYPE.mention:
                return this.renderMentionNotification(item);
            case NOTIFICATION_TYPE.story_accepted:
                return this.renderStoryNotification(item);
            case NOTIFICATION_TYPE.story_declined:
                return this.renderStoryNotification(item);
        }
    }

    componentWillReceiveProps(nextProps) {
        this.createDataSource(nextProps);
    }

    createDataSource({ notifications }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(notifications || []);
    }

    componentWillMount() {
        this.createDataSource(this.props);
    }

    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar title={this.props.title} openDrawer={this.props.openDrawer.bind(this)} />
                <ListView
                    dataSource={this.dataSource}
                    renderRow={this.renderItem.bind(this)}
                    style={styles.listN}
                    onEndReached={() => {
                        this.setState({ notifCount: this.state.notifCount + 8 }, () =>
                            this.props.getNotifications(this.props.uid, this.state.notifCount)
                        );
                    }}
                />
            </View>
        );
    }
}

const mapStateToProps = state => {
    let notifications = _.map(state.userInfo.notifications, n => {
        const time = moment(n.time, TIME_FORMAT).fromNow(false);
        return {
            type: n.type,
            name: n.likeAuthorName,
            action: NOTIFICATION_DESCRIPTION[n.type].action,
            target: n.postShort,
            time,
            smallIconName: NOTIFICATION_DESCRIPTION[n.type].icon,
            avatarImage: { uri: n.likeAuthorPhotoURL },
            uid: n.likeAuthorUid,
            postId: n.postId,
            targetComment: n.commentShort,
            targetCommentId: n.commentId
        };
    }).reverse();
    let uid = state.auth.user.uid;
    return { notifications, uid };
};

export default connect(mapStateToProps, { getNotifications })(NotificationList);
