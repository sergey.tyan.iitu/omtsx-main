/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar,
  Platform,
  ListView,
  Animated,
  Easing,
  TouchableOpacity,
  Dimensions,
  ScrollView
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';
import NavBar from '../../components/navBarAccount';
import Image from 'react-native-image-progress';

var styles = require('../../style.js');


const items = [
{
  nick: 'Almas_best',
  name: 'Алмас Ашимов',
  avatarImage: { uri:'https://placeimg.com/75/75/any' },
  subscribe: false
},
{
  nick: 'Vasya_007',
  name: 'Вася Пупкин',
  avatarImage: { uri:'https://placeimg.com/75/75/any' },
  subscribe: true
}

];


export default class omtsx extends Component {
  constructor(props) {
      super(props);

      this.state = {
        dataSource: new ListView.DataSource({
        rowHasChanged: (r1, r2) => r1 !== r2}),
        textInputValue: ''
      };
      

  }


  renderItem(item) {

      return(

        <View style={styles.itemSub}>

          

          <View style={styles.subAvatar}>
            <View style={styles.imgProfileSub}>
              <Image source={item.avatarImage} style={styles.itemImageSub} />
            </View>
          </View>
          <View style={styles.subscriberInfo}>
            <Text style={(item.name != '') ? styles.nameItemN : styles.nothing}>{item.nick}</Text>
            <Text style={styles.timeItemN}>{item.name} </Text>
          </View>

          <View style={styles.btnSub}>
          <TouchableOpacity
            underlayColor='rgba(0,0,0,0.05)'
            onPress={() => {}}
            style={styles.commentBtn}
            >
            <View></View>
          </TouchableOpacity>
            <Icon style={styles.subIcon} color='#8494a5'  name='md-checkmark' size={18}/>
            <Text style={styles.subText}>Подписка</Text>
          </View> 
 
        </View>
      )
  }


  componentDidMount() {
    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(items)
    });
  }


  render() {


    return (
      
      <View style={styles.containerNews}>
        <NavBar rightIcon='' title='Подписчики' openDrawer={this.props.openDrawer.bind(this)} />
        <ScrollView>
        <View style={styles.containerSubcribers}>
          <ListView
              dataSource={this.state.dataSource}
              renderRow={this.renderItem.bind(this)}
              style={styles.listCat}
              scrollEnabled={false}


          />
        </View>
 
      </ScrollView>
      </View>
    
    );
  }
}

