/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { View, StatusBar, Platform, ListView } from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import {
    fetchPublishedPostsByCat,
    stopWatchingPublishedPostsByCat
} from '../redux/actions';

import PostItem from './include/PostItem';
import Spinner from '../components/Spinner';
import NavBar from '../components/navBarAccount';

import * as styles from '../style.js';

class CategoryPosts extends Component {
    state = {
        postCount: 100
    };

    renderItem(item) {
        return <PostItem item={item} currentUser={this.props.currentUser} />;
    }

    componentDidMount() {
        StatusBar.setBarStyle('light-content', true);
    }

    renderLoading() {
        if (this.props.loading && Platform.OS === 'ios') {
            return <Spinner size="large" />;
        }
    }

    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar goBack title={this.props.category.name} />

                <View>
                    <ListView
                        dataSource={this.dataSource}
                        renderRow={this.renderItem.bind(this)}
                        style={styles.listS}
                        scrollbarStyle="outsideOverlay"
                        enableEmptySections
                        onEndReached={() => {
                            this.setState(
                                { postCount: this.state.postCount + 50 },
                                () =>
                                    this.props.fetchPublishedPostsByCat(
                                        this.state.postCount,
                                        this.props.category.catId
                                    )
                            );
                        }}
                    />

                    {this.renderLoading()}
                </View>
            </View>
        );
    }

    componentWillReceiveProps(nextProps) {
        this.createDataSource(nextProps);
    }

    createDataSource({ posts }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });

        this.dataSource = ds.cloneWithRows(posts);
    }

    componentWillMount() {
        this.props.fetchPublishedPostsByCat(
            this.state.postCount,
            this.props.category.catId
        );
        this.createDataSource(this.props);
    }

    componentWillUnmount() {
        this.props.stopWatchingPublishedPostsByCat(this.props.category.catId);
    }
}

const mapStateToProps = state => {
    let posts = _.map(state.posts.posted, (val, postId) => {
        return { ...val, postId };
    });

    let result = {
        posts,
        loading: state.posts.loading,
        currentUser: state.auth.user
    };
    return result;
};

export default connect(mapStateToProps, {
    fetchPublishedPostsByCat,
    stopWatchingPublishedPostsByCat
})(CategoryPosts);
