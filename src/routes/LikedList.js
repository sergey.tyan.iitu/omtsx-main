import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    Platform,
    ListView,
    TouchableOpacity
} from 'react-native';
import _ from 'lodash';
import { connect } from 'react-redux';
import * as styles from '../style.js';
import NavBar from '../components/navBarAccount';
import Spinner from '../components/Spinner';
import { fetchLikedList } from '../redux/actions';
import { Actions } from 'react-native-router-flux';

class LikedList extends Component {
    constructor() {
        super();
        this.state = {
            count: 10
        };
    }

    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar goBack title="Поставили лайк" />
                {this.renderLoading()}
                <ScrollView>
                    <View style={styles.containerCats}>
                        <ListView
                            enableEmptySections
                            scrollEnabled={false}
                            dataSource={this.dataSource}
                            renderRow={this.renderItem.bind(this)}
                            style={styles.listCat}
                            contentContainerStyle={{ alignItems: 'center' }}
                            onEndReached={() => {
                                this.setState(
                                    { count: this.state.count + 10 },
                                    () =>
                                        this.props.fetchLikedList(
                                            this.props.postId,
                                            this.state.count
                                        )
                                );
                            }}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }

    renderLoading() {
        if (this.props.loading && Platform.OS === 'ios') {
            return <Spinner size="large" />;
        }
    }

    renderItem(item) {
        return (
            <TouchableOpacity
                underlayColor="rgba(0,0,0,0.05)"
                onPress={() => Actions.accountPage({ userId: item.uid })}
            >
                <View style={styles.itemCat}>
                    <Text style={styles.catName}>{item.displayName}</Text>
                </View>
            </TouchableOpacity>
        );
    }

    createDataSource({ users }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(users || []);
    }

    componentWillReceiveProps(nextProps) {
        this.createDataSource(nextProps);
    }

    componentWillMount() {
        this.props.fetchLikedList(this.props.postId, this.state.count);
        this.createDataSource(this.props);
    }
}

const mapStateToProps = state => {
    const users = _.map(state.posts.likedList, (displayName, uid) => {
        return { displayName, uid };
    });
    return { users };
};

export default connect(mapStateToProps, {
    fetchLikedList
})(LikedList);
