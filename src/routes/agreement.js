/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { Text, View, ScrollView } from 'react-native';
import NavBar from '../components/navBarAccount';
import styles from '../style.js';

export default class AgreementPage extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar goBack title="Пользовательское соглашение" />
                <ScrollView style={styles.listS}>
                    <View style={styles.sectionAgree}>
                        <Text style={styles.agreeText}>
                            Lorem Ipsum - это текст-"рыба", часто используемый в
                            печати и вэб-дизайне. Lorem Ipsum является
                            стандартной "рыбой" для текстов на латинице с начала
                            XVI века. {'\n'}
                            {'\n'}В то время некий безымянный печатник создал
                            большую коллекцию размеров и форм шрифтов, используя
                            Lorem Ipsum для распечатки образцов. Lorem Ipsum не
                            только успешно пережил без заметных изменений пять
                            веков, но и перешагнул в электронный дизайн. {'\n'}
                            {'\n'}Его популяризации в новое время послужили
                            публикация листов Letraset с образцами Lorem Ipsum в
                            60-х годах и, в более недавнее время, программы
                            электронной вёрстки типа Aldus PageMaker, в шаблонах
                            которых используется Lorem Ipsum. Lorem Ipsum - это
                            текст-"рыба", часто используемый в печати и
                            вэб-дизайне. Lorem Ipsum является стандартной
                            "рыбой" для текстов на латинице с начала XVI века.{' '}
                            {'\n'}
                            {'\n'}В то время некий безымянный печатник создал
                            большую коллекцию размеров и форм шрифтов, используя
                            Lorem Ipsum для распечатки образцов. Lorem Ipsum не
                            только успешно пережил без заметных изменений пять
                            веков, но и перешагнул в электронный дизайн. {'\n'}
                            {'\n'}Его популяризации в новое время послужили
                            публикация листов Letraset с образцами Lorem Ipsum в
                            60-х годах и, в более недавнее время, программы
                            электронной вёрстки типа Aldus PageMaker, в шаблонах
                            которых используется Lorem Ipsum. Lorem Ipsum - это
                            текст-"рыба", часто используемый в печати и
                            вэб-дизайне. Lorem Ipsum является стандартной
                            "рыбой" для текстов на латинице с начала XVI века.{' '}
                            {'\n'}
                            {'\n'}В то время некий безымянный печатник создал
                            большую коллекцию размеров и форм шрифтов, используя
                            Lorem Ipsum для распечатки образцов. Lorem Ipsum не
                            только успешно пережил без заметных изменений пять
                            веков, но и перешагнул в электронный дизайн. {'\n'}
                            {'\n'}Его популяризации в новое время послужили
                            публикация листов Letraset с образцами Lorem Ipsum в
                            60-х годах и, в более недавнее время, программы
                            электронной вёрстки типа Aldus PageMaker, в шаблонах
                            которых используется Lorem Ipsum.
                        </Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}
