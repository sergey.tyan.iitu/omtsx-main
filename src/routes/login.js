/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
    Animated,
    Platform,
    StatusBar,
    Text,
    TextInput,
    TouchableHighlight,
    TouchableNativeFeedback,
    View
} from 'react-native';
import { connect } from 'react-redux';
import { emailChanged, loginUser, passwordChanged } from '../redux/actions';
import { Actions } from 'react-native-router-flux';

var styles = require('../style.js');

class LoginForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            text: 'Useless Placeholder'
        };
    }

    onEmailChange(text) {
        this.props.emailChanged(text);
    }

    onPasswordChange(text) {
        this.props.passwordChanged(text);
    }

    onLoginButtonPress() {
        const { email, password } = this.props;

        this.props.loginUser({ email, password });
    }

    componentWillMount() {
        this.offsetTop = new Animated.Value(0);
    }

    componentDidMount() {
        StatusBar.setBarStyle('light-content', true);
    }

    offsetFormTop() {
        Animated.timing(this.offsetTop, {
            toValue: -140,
            duration: 200
        }).start();
    }

    offsetFormNormal() {
        Animated.timing(this.offsetTop, {
            toValue: 0,
            duration: 200
        }).start();
    }

    render() {
        const TouchableElement =
            Platform.OS === 'android'
                ? TouchableNativeFeedback
                : TouchableHighlight;

        const offsetTopStyle = { marginTop: this.offsetTop };
        return (
            <View style={styles.container}>
                <Animated.View style={[styles.center, offsetTopStyle]}>
                    <Text style={styles.logo}>OMTSX</Text>
                    <TextInput
                        style={styles.input}
                        placeholder="Имя пользователя"
                        onFocus={this.offsetFormTop.bind(this)}
                        onBlur={this.offsetFormNormal.bind(this)}
                        placeholderTextColor="#c7aeb2"
                        underlineColorAndroid="transparent"
                        onChangeText={this.onEmailChange.bind(this)}
                    />
                    <TextInput
                        style={styles.input}
                        placeholder="Пароль"
                        onFocus={this.offsetFormTop.bind(this)}
                        onBlur={this.offsetFormNormal.bind(this)}
                        placeholderTextColor="#c7aeb2"
                        secureTextEntry={true}
                        underlineColorAndroid="transparent"
                        onChangeText={this.onPasswordChange.bind(this)}
                    />

                    <TouchableElement
                        underlayColor="#704140"
                        style={styles.width100}
                        onPress={this.onLoginButtonPress.bind(this)}
                    >
                        <View style={styles.regbutton}>
                            <Text style={styles.loginText}>Войти</Text>
                        </View>
                    </TouchableElement>

                    <Text style={styles.forgot}>Забыли данные для входа?</Text>
                    <TouchableElement
                        underlayColor="transparent"
                        onPress={() => Actions.forgot()}
                    >
                        <Text style={styles.helpLogin}>Помощь со входом.</Text>
                    </TouchableElement>
                </Animated.View>

                <View style={styles.footer}>
                    <Text style={styles.regText}>У Вас нет аккаунта?</Text>
                    <TouchableElement
                        underlayColor="transparent"
                        onPress={() => Actions.signup()}
                    >
                        <Text style={styles.regDo}>Зарегистриуйтесь.</Text>
                    </TouchableElement>
                </View>
            </View>
        );
    }
}

const mapStateToProps = ({ auth }) => {
    const { email, password, error, loading } = auth;

    return { email, password, error, loading };
};

export default connect(mapStateToProps, {
    emailChanged,
    passwordChanged,
    loginUser
})(LoginForm);
