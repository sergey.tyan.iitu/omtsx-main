/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import { Text, View, TouchableOpacity, ListView, ScrollView, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import * as firebase from 'firebase';
import Icon from 'react-native-vector-icons/Ionicons';

import { loadCategories } from '../redux/actions';
import Menu from '../components/drawMenu';
import NavBar from '../components/navBarAccount';
import * as styles from '../style.js';

const itemsFilter = [
    {
        name: 'Только от парней'
    },
    {
        name: 'Только от девушек'
    },
    {
        name: 'Только с фото'
    }
];

const catSelectStyles = StyleSheet.create({
    item: {
        flexDirection: 'row',
        height: 45,
        borderRadius: 5,
        alignItems: 'center',
        marginLeft: 10,
        marginRight: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#ecebec'
    }
});

class CatSelect extends Component {
    constructor(props) {
        super(props);

        this.state = {
            dataSourceCat: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            }),
            dataSourceFilter: new ListView.DataSource({
                rowHasChanged: (r1, r2) => r1 !== r2
            }),
            activeCategories: [],
            selectFilter: []
        };
        this.userFilterRef = firebase.database().ref(`user-info/${this.props.currentUser.uid}`);
        this.renderCategory = this.renderCategory.bind(this);
        this.updateCategoriesInFirebase = this.updateCategoriesInFirebase.bind(this);
    }

    selectCat(rowID) {
        const index = this.props.filteredCategories.indexOf(rowID);

        if (index === -1) {
            this.setState(
                {
                    activeCategories: [...this.props.filteredCategories, rowID]
                },
                this.updateCategoriesInFirebase
            );
        } else {
            this.setState(
                {
                    activeCategories: [this.props.filteredCategories.filter(item => item !== rowID)]
                },
                this.updateCategoriesInFirebase
            );
        }
    }

    selectFilter(rowID) {
        if (this.state.selectFilter.indexOf(rowID) === -1) {
            const arrayFilter = this.state.selectFilter;
            arrayFilter.push(rowID);
            this.setState({
                selectFilter: arrayFilter
            });
        } else {
            const arrayFilter = this.state.selectFilter;
            delete arrayFilter[this.state.selectFilter.indexOf(rowID)];
            this.setState({
                selectFilter: arrayFilter
            });
        }
    }

    componentDidMount() {
        this.setState({
            dataSourceCat: this.state.dataSourceCat.cloneWithRows(this.props.cats),
            dataSourceFilter: this.state.dataSourceFilter.cloneWithRows(itemsFilter)
        });
    }

    renderCategory(item, sectionID, rowID) {
        return (
            <TouchableOpacity underlayColor="rgba(0,0,0,0.1)" onPress={() => this.selectCat(rowID)}>
                <View style={catSelectStyles.item}>
                    <Text style={styles.catName}>{item.name}</Text>

                    {this.props.filteredCategories.indexOf(rowID) === -1 && (
                        <Icon style={styles.catIcon} color={'#8c98ab'} name="md-checkmark" size={18} />
                    )}
                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar goBack title="Выбор категорий" />
                <ScrollView>
                    <View style={styles.sectionLabelFirst}>
                        <Text style={styles.sectionLabelText}>Выберите категории публикации</Text>
                    </View>
                    <View style={styles.section}>
                        <ListView
                            enableEmptySections
                            scrollEnabled={false}
                            dataSource={this.state.dataSourceCat}
                            renderRow={this.renderCategory}
                        />
                    </View>

                    <View style={styles.sectionLabel}>
                        <Text style={styles.sectionLabelText}>Фильтры</Text>
                    </View>
                    <View style={styles.section}>
                        <ListView
                            scrollEnabled={false}
                            dataSource={this.state.dataSourceFilter}
                            renderRow={(item, sectionID, rowID) => (
                                <TouchableOpacity
                                    underlayColor="rgba(0,0,0,0.1)"
                                    onPress={() => this.selectFilter(rowID)}
                                >
                                    <View style={catSelectStyles.item}>
                                        <Text style={styles.catName}>{item.name}</Text>
                                        <Icon
                                            style={styles.catIcon}
                                            color={this.state.selectFilter.indexOf(rowID) === -1 ? '#fff' : '#8c98ab'}
                                            name="md-checkmark"
                                            size={18}
                                        />
                                    </View>
                                </TouchableOpacity>
                            )}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            dataSourceCat: this.state.dataSourceCat.cloneWithRows(nextProps.cats),
            activeCategories: nextProps.filteredCategories
        });
    }

    updateCategoriesInFirebase() {
        this.userFilterRef.update({ 'categories-filter': this.state.activeCategories.join(',') });
    }

    componentWillMount() {
        if (this.props.cats.length === 0) {
            this.props.loadCategories();
        }
    }
}

const mapStateToProps = state => {
    const cats = _.map(state.categories.all, (val, catId) => {
        return { ...val, catId };
    });
    let result = { cats, currentUser: state.auth.user, filteredCategories: state.userInfo.filteredCategories };
    return result;
};

CatSelect = connect(mapStateToProps, { loadCategories })(CatSelect);

export default class CatSelectWithMenu extends Component {
    render() {
        return <Menu title={this.props.title} component={CatSelect} />;
    }
}
