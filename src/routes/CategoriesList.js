import React, { Component } from 'react';
import {
    Text,
    View,
    ScrollView,
    Platform,
    ListView,
    TouchableOpacity
} from 'react-native';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';
import * as styles from '../style.js';
import NavBar from '../components/navBar';
import Spinner from '../components/Spinner';
import { loadCategories } from '../redux/actions';
import { Actions } from 'react-native-router-flux';

class CategoriesList extends Component {
    render() {
        return (
            <View style={styles.containerNews}>
                <NavBar openDrawer={this.props.openDrawer.bind(this)} />
                {this.renderLoading()}
                <ScrollView>
                    <View style={styles.containerCats}>
                        <ListView
                            enableEmptySections
                            scrollEnabled={false}
                            dataSource={this.dataSource}
                            renderRow={this.renderItem.bind(this)}
                            style={styles.listCat}
                            contentContainerStyle={{ alignItems: 'center' }}
                        />
                    </View>
                </ScrollView>
            </View>
        );
    }

    renderLoading() {
        if (this.props.loading && Platform.OS === 'ios') {
            return <Spinner size="large" />;
        }
    }

    renderItem(item) {
        return (
            <TouchableOpacity
                underlayColor="rgba(0,0,0,0.05)"
                onPress={() => Actions.categoryPost({ category: item })}
            >
                <View style={styles.itemCat}>
                    <Text style={styles.catName}>{item.name}</Text>
                    <Icon
                        style={{}}
                        color="#8c98ab"
                        name="ios-arrow-forward"
                        size={18}
                    />
                </View>
            </TouchableOpacity>
        );
    }

    createDataSource({ cats }) {
        const ds = new ListView.DataSource({
            rowHasChanged: (r1, r2) => r1 !== r2
        });
        this.dataSource = ds.cloneWithRows(cats);
    }

    componentWillReceiveProps(nextProps) {
        this.createDataSource(nextProps);
    }

    componentWillMount() {
        if (this.props.cats.length === 0) {
            this.props.loadCategories();
        }
        this.createDataSource(this.props);
    }
}

const mapStateToProps = state => {
    const cats = _.map(state.categories.all, (val, catId) => {
        return { ...val, catId };
    });

    let result = { cats, loading: state.categories.loading };
    return result;
};

export default connect(mapStateToProps, {
    loadCategories
})(CategoriesList);
