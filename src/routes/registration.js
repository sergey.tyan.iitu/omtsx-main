/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
import React, { Component } from 'react';
import {
    Text,
    View,
    StatusBar,
    TextInput,
    TouchableOpacity,
    TouchableNativeFeedback,
    Platform,
    Image,
    Animated,
    StyleSheet,
    Switch
} from 'react-native';
import { connect } from 'react-redux';
import { Actions } from 'react-native-router-flux';
import Icon from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-picker';
import {
    emailChanged,
    imageUriChanged,
    passwordChanged,
    registerUser,
    usernameChanged
} from '../redux/actions/RegistrationActions';

import { mainColor2, width } from '../constants';

import styles from '../style.js';

const registrationStyle = StyleSheet.create({
    switchContainer: {
        height: 50,
        width: width - 30,
        backgroundColor: mainColor2,
        borderRadius: 6,
        marginBottom: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20
    },
    switchLabel: {
        color: '#fff',
        fontSize: 14
    }
});

class RegistrationForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            image: null,
            genderMale: false
        };
    }

    showImagePicker() {
        ImagePicker.showImagePicker(null, response => {
            if (response.uri) {
                let source = { uri: response.uri };
                this.setState({ image: source });
                this.props.imageUriChanged(response.uri);
            }
        });
    }

    onEmailChange(text) {
        this.props.emailChanged(text);
    }

    onPasswordChange(text) {
        this.props.passwordChanged(text);
    }

    onUsernameChange(text) {
        this.props.usernameChanged(text);
    }

    onRegisterUser() {
        const { username, email, password, imageUri } = this.props;

        if (username && email && password) {
            this.props.registerUser(
                username,
                email,
                password,
                imageUri,
                this.state.genderMale ? 'M' : 'F'
            );
        }
    }

    componentWillMount() {
        this.offsetTop = new Animated.Value(0);
    }

    componentDidMount() {
        StatusBar.setBarStyle('light-content', true);
    }

    offsetFormTop() {
        Animated.timing(this.offsetTop, {
            toValue: -100,
            duration: 200
        }).start();
    }

    offsetFormNormal() {
        Animated.timing(this.offsetTop, {
            toValue: 0,
            duration: 200
        }).start();
    }

    renderImageOrButton() {
        if (this.state.image) {
            return (
                <Image
                    source={this.state.image}
                    resizeMode={'contain'}
                    style={styles.uploadedImage}
                />
            );
        } else {
            return (
                <View style={styles.avatar}>
                    <Image
                        style={styles.avatarImg}
                        source={require('../images/cat.png')}
                    />
                    <Icon
                        name="ios-add-outline"
                        size={50}
                        style={styles.avatarIcon}
                        color="#fff"
                    />
                </View>
            );
        }
    }

    render() {
        const TouchableElement =
            Platform.OS === 'android'
                ? TouchableNativeFeedback
                : TouchableOpacity;

        const offsetTopStyle = { marginTop: this.offsetTop };
        return (
            <View style={styles.container}>
                <Animated.View style={[styles.center, offsetTopStyle]}>
                    <TouchableElement onPress={this.showImagePicker.bind(this)}>
                        {this.renderImageOrButton()}
                    </TouchableElement>
                    <TextInput
                        style={styles.reginput}
                        placeholder="Имя пользователя"
                        placeholderTextColor="#c7aeb2"
                        onFocus={this.offsetFormTop.bind(this)}
                        onBlur={this.offsetFormNormal.bind(this)}
                        underlineColorAndroid="transparent"
                        onChangeText={this.onUsernameChange.bind(this)}
                    />
                    <TextInput
                        style={styles.reginput}
                        placeholder="E-mail"
                        placeholderTextColor="#c7aeb2"
                        onFocus={this.offsetFormTop.bind(this)}
                        onBlur={this.offsetFormNormal.bind(this)}
                        underlineColorAndroid="transparent"
                        onChangeText={this.onEmailChange.bind(this)}
                        keyboardType={'email-address'}
                    />
                    <TextInput
                        style={styles.reginput}
                        placeholder="Пароль"
                        placeholderTextColor="#c7aeb2"
                        onFocus={this.offsetFormTop.bind(this)}
                        onBlur={this.offsetFormNormal.bind(this)}
                        secureTextEntry={true}
                        underlineColorAndroid="transparent"
                        onChangeText={this.onPasswordChange.bind(this)}
                    />

                    <View style={registrationStyle.switchContainer}>
                        <Text style={registrationStyle.switchLabel}>
                            {this.state.genderMale
                                ? 'Пол: мужской'
                                : 'Пол: женский'}
                        </Text>
                        <Switch
                            tintColor={'pink'}
                            onTintColor={'#42aaf4'}
                            thumbTintColor={'white'}
                            value={this.state.genderMale}
                            onValueChange={() =>
                                this.setState({
                                    genderMale: !this.state.genderMale
                                })}
                        />
                    </View>

                    <TouchableElement
                        underlayColor="#704140"
                        onPress={this.onRegisterUser.bind(this)}
                        style={styles.width100}
                    >
                        <View style={styles.regbutton}>
                            <Text style={styles.loginText}>Далее</Text>
                        </View>
                    </TouchableElement>

                    <Text style={styles.sogl}>
                        Регистрируясь, Вы соглашаетесь с нашими
                    </Text>

                    <TouchableElement
                        underlayColor="transparent"
                        onPress={() => {
                            Actions.agreement2();
                        }}
                    >
                        <Text style={styles.usl}>
                            Условиями и политикой конфиденциальности.
                        </Text>
                    </TouchableElement>
                </Animated.View>

                <View style={styles.footer}>
                    <Text style={styles.regText}>У Вас есть аккаунт?</Text>

                    <TouchableElement
                        underlayColor="transparent"
                        onPress={() => Actions.pop()}
                    >
                        <Text style={styles.regDo}>Выполните вход.</Text>
                    </TouchableElement>
                </View>
            </View>
        );
    }
}

const mapStateToProps = ({ registration }) => {
    const {
        imageUri,
        email,
        password,
        username,
        user,
        loading,
        error
    } = registration;

    return {
        imageUri,
        email,
        password,
        username,
        user,
        loading,
        error
    };
};

export default connect(mapStateToProps, {
    emailChanged,
    imageUriChanged,
    passwordChanged,
    usernameChanged,
    registerUser
})(RegistrationForm);
